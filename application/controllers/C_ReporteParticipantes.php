<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_ReporteParticipantes extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('excel');
	}

	public function index()
	{
		$this->Excel();
	}

	/*
	public function PDF(){
		$this->load->model('verano_digital/M_Reportes');
		$data['ReporteParticipantes'] = $this->M_Reportes->ObtenerReporteParticipantes();
		$this->load->view('verano_digital/V_ReporteParticipantes_PDF.php', $data);
	}
	*/

	public function Excel(){
		$this->load->model('verano_digital/M_Reportes');
		$ReporteParticipantes = $this->M_Reportes->ObtenerReporteParticipantes();

		//print_r($ReporteParticipantes);

		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->getPageMargins()->setHeader(0);
		$this->excel->getActiveSheet()->getPageMargins()->setTop(1.2);

		$this->excel->getActiveSheet()->getHeaderFooter()->setOddFooter('Página &P de &N');
		$this->excel->setActiveSheetIndex(0);

					$estiloEncabezadosColumnas = array(
						'font' => array(
								'name'   => 'Verdana',
								'bold'   => true,
								'italic' => false,
								'strike' => false,
								'size'	 => 10,
								'color' => array(
									'rgb'	=> '000000'
								)
						 ),

						'borders' => array(
							'allborders' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN                    
							)
						),

						'fill' => array(
								'type' => PHPExcel_Style_Fill::FILL_SOLID,
								'color' => array('rgb' => 'D9E3FF')
						)
					);
		
					$estiloCuerpoColumnas = array(
						'font' => array(
							'name'      => 'Arial',
							'bold'      => false,                          
							'color'     => array(
								'rgb'   => '000000'
							)
						),
						'borders' => array(
							'outline' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN                    
							),
						)
					);    
					
		// Aplica el diseño a todas las columnas
		$this->excel->getActiveSheet()->getStyle('A1:P1')->applyFromArray($estiloEncabezadosColumnas);
		
		// Auto Size
		for($i = 'A'; $i !== 'Q'; $i++) {
			$this->excel->getActiveSheet()->getColumnDimension( $i )->setAutoSize(true);	
		}
																				
		//nombre de la hoja
		$this->excel->getActiveSheet()->setTitle('Participantes');

		//encabezados de la tabla
		$filaEncabezados = 1;
		
		//foreach ($data_encabezados as $key => $value) {
		$this->excel->getActiveSheet()
				->setCellValue('A'.$filaEncabezados, 'Id')
				->setCellValue('B'.$filaEncabezados, 'Nombre')
				->setCellValue('C'.$filaEncabezados, 'Ap_pa')
				->setCellValue('D'.$filaEncabezados, 'Ap_ma')
				->setCellValue('E'.$filaEncabezados, 'Genero')
				->setCellValue('F'.$filaEncabezados, 'Municipio')
				->setCellValue('G'.$filaEncabezados, 'Localidad')
				->setCellValue('H'.$filaEncabezados, 'nuevoMunicipio')
				->setCellValue('I'.$filaEncabezados, 'nuevoPoblado')
				->setCellValue('J'.$filaEncabezados, 'Escuela')
				->setCellValue('K'.$filaEncabezados, 'Grado')
				->setCellValue('L'.$filaEncabezados, 'Email')
				->setCellValue('M'.$filaEncabezados, 'Email_tutor')
				->setCellValue('N'.$filaEncabezados, 'Telefono')
				->setCellValue('O'.$filaEncabezados, 'Fecha de nacimiento')
				->setCellValue('P'.$filaEncabezados, 'Fecha y hora de registro')
		;

		$filaContenido = 2;
		foreach ($ReporteParticipantes as $val){                  
			$this->excel->getActiveSheet()
				->setCellValue('A'.$filaContenido, $val['Id'])
				->setCellValue('B'.$filaContenido, $val['Nombre'])
				->setCellValue('C'.$filaContenido, $val['Ap_pa'])
				->setCellValue('D'.$filaContenido, $val['Ap_ma'])
				->setCellValue('E'.$filaContenido, $val['Genero'])
				->setCellValue('F'.$filaContenido, $val['Municipio'])
				->setCellValue('G'.$filaContenido, $val['Localidad'])
				->setCellValue('H'.$filaContenido, $val['nuevoMunicipio'])
				->setCellValue('I'.$filaContenido, $val['nuevoPoblado'])
				->setCellValue('J'.$filaContenido, $val['Escuela'])
				->setCellValue('K'.$filaContenido, $val['Grado'])
				->setCellValue('L'.$filaContenido, $val['Email'])
				->setCellValue('M'.$filaContenido, $val['Email_tutor'])
				->setCellValue('N'.$filaContenido, $val['Telefono'])
				->setCellValue('O'.$filaContenido, $val['Fecha de nacimiento'])
				->setCellValue('P'.$filaContenido, $val['Fecha y hora de registro'])
			;
			$filaContenido++;
		}
		
		for($i = 'A'; $i !== 'Q'; $i++) {
			$this->excel->getActiveSheet()->getStyle( $i.'2:'.$i.($filaContenido-1) )->applyFromArray($estiloCuerpoColumnas);
		}

		// No se, hay que investigar
		$this->excel->getActiveSheet()->fromArray($ReporteParticipantes, null, 'A2');
																						 
		$filename='Reporte_DetallesDeParticipantes.xls'; //nombre del archivo
																						
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //nombre del archivo
		header('Cache-Control: max-age=0'); //no cache

		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output'); 
	}

}

/* End of file C_ReporteParticipantes.php */
/* Location: ./application/controllers/C_ReporteParticipantes.php */