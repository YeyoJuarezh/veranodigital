<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Welcome extends CI_Controller {

	private $inicial = 0;
	private $final = 0;
	private $registros = 0;

	function __construct() {
		parent::__construct();
		$this->load->model('M_main');
	}

	public function index() {
		if($this->session->userdata('id'))
		{
			$this->login();
		}
		else
		{
			$this->load->view('plantilla/header');
			$this->load->view('plantilla/nav');
			$this->load->view('plantilla/login');
			$this->load->view('plantilla/footer');
		}
	}

	public function login() {
		$usuario = $this->input->post('usuario');
		$password = (sha1($this->input->post('password')));
		$this->M_main->login($usuario, $password);

		if ($this->session->userdata('id')) {
			switch ($this->session->userdata('rol')) {
				case 1:		redirect(base_url('Registros/C_Administrador'));		break;
				case 2:		redirect(base_url('Registros/C_Coordinador'));			break;
				case 3:		redirect(base_url('Registros/C_Director'));					break;
				case 4:		redirect(base_url('Registros/C_Laboratorista'));		break;
			}
		} else {
			$this->index();
		}

	}

	public function olvidePassword() {
		//$data_nav['menu_inicial'] = $this->M_querys->consulta('*', 'rvoe_catmenuinicial');
		//$data_nav['submenu_inicial'] = $this->M_querys->consulta('*', 'rvoe_docinicial');

		$this->load->view('plantilla/header');
		$this->load->view('plantilla/nav');
		$this->load->view('plantilla/V_recuperaPassword');
		$this->load->view('plantilla/footer');
	}

	public function nuevoPassword() {

		foreach ($_POST as $indice => $valor) {
			$data[$indice] = $this->input->post($indice);
		}

		$valida = $this->M_querys->consulta('*', 'infor_usuario', null, 'correo = "' . $data['correo'] . '"');

		if (sizeof($valida) > 0) {
			foreach ($valida as $indice) {
				$nombre = $indice['nombre'];
				$data_msj['correo'] = $indice['correo'];
				$data_msj['id'] = $indice['id'];
				$data['mensaje_vista'] = '<div class="alert alert-info alert-dismissable">
																		<strong>Nota!</strong> Se ha generado su nueva contraseña, por favor verifique su correo electrónico para confirmar.
																	</div>';
			}

			$data_msj['pass'] = $this->l_utilerias->randomString(7);
			$data_msj['asunto'] = 'Recuperación de Contraseña';
			$data_msj['mensaje'] = $nombre . ':<br><br>Le informamos que su contraseña para acceder al sistema fue actualizada el día ' . date('Y-m-d H:i:s') . '.<br><br>Contraseña > '.$data_msj['pass'];

			$this->M_querys->guardar('infor_usuario', array('password' => (sha1($data_msj['pass']))), $data_msj['id'], "id");
			$this->mail->enviarMensaje($data_msj);
		} else {
			$data['mensaje_vista'] = '<div class="alert alert-warning alert-dismissable">
																	<button type="button" class="close" data-dismiss="alert">&times;</button>
																	<strong>Nota!</strong> El correo que ingreso no se encuentra en la base de datos.
																</div>';
			}

			// $data_nav['menu_inicial'] = $this->M_querys->consulta('*', 'rvoe_catmenuinicial');
			// $data_nav['submenu_inicial'] = $this->M_querys->consulta('*', 'rvoe_docinicial');

			$this->load->view('plantilla/header');
			$this->load->view('plantilla/nav');
			$this->load->view('plantilla/V_recuperaPassword', $data);
			$this->load->view('plantilla/footer');
	}

	public function destroyCt() {
		//$metodo que elimina el centro de trabajo seleccionado, una ves eliminado direccionaremos al metodo index.

		$data = array('ct' => '');
		$this->session->set_userdata($data);
		redirect(base_url('Regitros/C_Laboratorista'));
	}

	public function validaLogin() {
		if($this->session->userdata('id')) {
			echo 1;
		}else {
			echo 0;
		}
	}

	public function logout() {
		unset($usuario);
		unset($password);
		$this->session->sess_destroy();
		//redirect(base_url());
	}
}