<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Inicio extends CI_Controller {

	function __construct() {
		parent::__construct();
		//$this->load->helper('menuNav');
	}

	public function index()
	{
		$data['titulo'] = substr($this->uri->segment(1), 2);
		if($data['titulo'] == ''){	$data['titulo'] = 'Inicio';	}

		$this->load->view('plantilla/header');
		//$this->load->view('plantilla/nav');
			$this->load->view('verano_digital/V_VDHeader.php');
			$this->load->view('verano_digital/V_VDNav.php', $data);
			$this->load->view('verano_digital/V_'.$data['titulo'].'.php', $data);
			$this->load->view('verano_digital/V_VDFooter.php');
		$this->load->view('plantilla/footer');
	}

}

/* End of file c_Inicio.php */
/* Location: ./application/controllers/c_Inicio.php */