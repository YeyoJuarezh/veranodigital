<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Galeria extends CI_Controller {

	function __construct() {
		parent::__construct();
		//$this->load->helper('menuNav');
	}

	public function index()
	{
		$data['titulo'] = substr($this->uri->segment(1), 2);
			
		$this->load->view('plantilla/header');
		//$this->load->view('plantilla/nav');
			$this->load->view('verano_digital/V_VDHeader.php');
			$this->load->view('verano_digital/V_VDNav.php', $data);
			$this->load->view('verano_digital/V_'.$data['titulo'].'.php', $data);
			$this->load->view('verano_digital/V_VDFooter.php');
		$this->load->view('plantilla/footer');
		$this->load->view('modals/VM_Galeria.php', $data);
	}

	public function Obtener_Imagenes(){
			$imagenes = array();
			$img_dir 	= dirname(dirname(dirname(
										realpath(__FILE__)
									))) . '/assets/img/verano_digital/Galeria/';
			$handle 	= opendir( $img_dir );

			while($file = readdir($handle)){
					if($file !== '.' && $file !== '..'){
							array_push($imagenes, $file);
					}
			}
			// echo json_encode($img_dir);
			echo json_encode($imagenes);
	}
}

/* End of file c_Inicio.php */
/* Location: ./application/controllers/c_Inicio.php */