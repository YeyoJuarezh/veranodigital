<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Registro extends CI_Controller {

	function __construct() {
		parent::__construct();
		//$this->load->helper('menuNav');
	}

	public function index()
	{
		$data['titulo'] = substr($this->uri->segment(1), 2);

		$this->load->view('plantilla/header');
		//$this->load->view('plantilla/nav');
			$this->load->view('verano_digital/V_VDHeader.php');
			$this->load->view('verano_digital/V_VDNav.php', $data);

			$this->load->model('verano_digital/M_Registro');
				$R_sexos 			= $this->M_Registro->ObtenerSexo();
				$R_municipios = $this->M_Registro->ObtenerMunicipios();
				$R_localidades= $this->M_Registro->ObtenerLocalidades();
				$R_escuelas 	= $this->M_Registro->ObtenerEscuelas();
				
			$this->load->library('Verano_digital');
				$data['sexos'] 			= $this->verano_digital->construirSelect($R_sexos);
				$data['municipios'] = $this->verano_digital->construirSelect($R_municipios);
				//$data['poblados'] 	= $this->verano_digital->construirSelect($R_localidades);
				$data['escuelas'] 	= $this->verano_digital->construirSelect($R_escuelas);

			$this->load->view('verano_digital/V_'.$data['titulo'].'.php', $data);
			$this->load->view('verano_digital/V_VDFooter.php');
		$this->load->view('plantilla/footer');
	}

	public function ObtenerLocalidades()
	{
		$this->load->model('verano_digital/M_Registro');
		$registroInfo = $this->input->post();
		$R_localidades 	= $this->M_Registro->ObtenerLocalidades($registroInfo);
		
		$this->load->library('verano_digital');
		$data_set 		= $this->verano_digital->construirSelect($R_localidades);

		//Either you can print value or you can send value to database
		echo json_encode($data_set);
	}

	public function ObtenerEscuelas()
	{
		$this->load->model('verano_digital/M_Registro');
		$registroInfo = $this->input->post();
		$R_escuelas 	= $this->M_Registro->ObtenerEscuelas($registroInfo);
		
		$this->load->library('verano_digital');
		$data_set 		= $this->verano_digital->construirSelect($R_escuelas);

		//Either you can print value or you can send value to database
		echo json_encode($data_set);
	}

	public function ConsultarParticipanteDuplicado(){
		$this->load->model('verano_digital/M_Registro');
		$registroInfo = $this->input->post();
		$resultado 		= $this->M_Registro->consultarSiYaSeRegistro($registroInfo);

		//Either you can print value or you can send value to database
		echo json_encode($resultado);
	}

	public function Registrar()
	{
		$this->load->model('verano_digital/M_Registro');
		$registroInfo = $this->input->post();
		$resultado 		= $this->M_Registro->Registrar($registroInfo);

		//Either you can print value or you can send value to database
		echo json_encode($resultado);
	}
}

/* End of file c_Inicio.php */
/* Location: ./application/controllers/c_Inicio.php */