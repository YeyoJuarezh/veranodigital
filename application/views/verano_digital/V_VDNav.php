<?php 
function obtenerNav()
{
	return array(
		array('href' => 'C_Inicio',		'src' => 'Inicio',		'data-hover' => 'Inicio_hover',		'alt' => 'Inicio'   ),
		array('href' => 'C_QueEs',		'src' => 'QueEs',			'data-hover' => 'QueEs_hover',		'alt' => '¿Qué es?' ),
		array('href' => 'C_Galeria',	'src' => 'Galeria',		'data-hover' => 'Galeria_hover',	'alt' => 'Galeria'	),
		array('href' => 'C_Contacto',	'src' => 'Contacto',	'data-hover' => 'Contacto_hover',	'alt' => 'Contacto'	),
		array('href' => 'C_Registro',	'src' => 'Registro',	'data-hover' => 'Registro_hover',	'alt' => 'Registro'	)
	);
}
//_hover
?>

<nav class="container-fluid">
	<div class="row">
		<?php foreach (obtenerNav() as $elementoMenu): ?>
			<?php if($elementoMenu['src'] == 'Inicio'): ?>
				<div class="col-xs-2 col-xs-offset-1">
			<?php else: ?>
				<div class="col-xs-2">
			<?php endif; ?>
					<a href="<?= base_url().$elementoMenu['href']; ?>">
						<img class="imagenes navButtons" 
						<?php if($elementoMenu['src'] == $titulo): ?>
								data-src="<?= base_url();?>assets/img/verano_digital/botones_menu/<?= $elementoMenu['data-hover']; ?>.png" 
							data-hover="<?= base_url();?>assets/img/verano_digital/botones_menu/<?= $elementoMenu['src']; ?>.png" 
										src="<?= base_url();?>assets/img/verano_digital/botones_menu/<?= $elementoMenu['data-hover']; ?>.png" 
						<?php else: ?>
								data-src="<?= base_url();?>assets/img/verano_digital/botones_menu/<?= $elementoMenu['src']; ?>.png" 
							data-hover="<?= base_url();?>assets/img/verano_digital/botones_menu/<?= $elementoMenu['data-hover']; ?>.png" 
										src="<?= base_url();?>assets/img/verano_digital/botones_menu/<?= $elementoMenu['src']; ?>.png" 
						<?php endif; ?>
					 	alt='Botón de menú que lleva a la sección "<?= $elementoMenu['alt']; ?>"'>
					</a>
				</div>
		<?php endforeach; ?>
	</div>
</nav>

<main class="container-fluid">
	<div class="row">