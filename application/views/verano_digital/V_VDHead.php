<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title><?= $titulo; ?></title>
	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
	
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/verano_digital.css">
</head>
<body>