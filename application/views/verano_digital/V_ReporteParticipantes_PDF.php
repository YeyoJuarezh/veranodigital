<?php
//============================================================+
// File name   : example_048.php
// Begin       : 2009-03-20
// Last Update : 2013-05-14
//
// Description : Example 048 for TCPDF class
//               HTML tables and table headers
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               ReporteParticipantes@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: HTML tables and table headers
 * @author Nicola Asuni
 * @since 2009-03-20
 */

// Include the main TCPDF library (search for installation path).
include("system/libraries/tcpdf/fonts/times.php"); //tcpdf
include("system/libraries/tcpdf/tcpdf.php"); //tcpdf

$this->load->library('tcpdf'); //tcpdf
	
class MYPDF extends TCPDF {
	
	//Page header
	public function Header() {

		// Set font
		$this->SetFont('helvetica', '', 8);

		// Title
		$html = '
			<table cellspacing="0" cellpadding="1" border="0">
				<tr>
					<td width="200" rowspan="3"> 22 </td>
					<td width="660" align="center">SECRETARÍA DE EDUCACIÓN DE TAMAULIPAS</td>
					<td width="200"></td>
				</tr>
				<tr>
					<td width="660" align="center">COORDINACIÓN DE BECAS Y ESTÍMULOS EDUCATIVOS</td>
					<td width="200"></td>
				</tr>
				<tr>
					<td width="680" align="center"><b>REPORTE "PROGRAMA INTEGRACIÓN" CICLO ESCOLAR 2017-2018</b></td>
					<td width="200"></td>
				</tr>
			</table>';

		$this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
		$html1 ="";
			//array_keys($info)
			//count($ReporteParticipantes)

		$html1 .='
			<table>
				<tr>
					<th width="40"  style="border-bottom:1px solid #000;" align="center"><b>No.</b></th>
					<th width="130" style="border-bottom:1px solid #000;" align="center"><b>CURP</b></th>
					<th width="400" style="border-bottom:1px solid #000;"><b>NOMBRE ALUMNO</b></th>
					<th width="100" style="border-bottom:1px solid #000;"><b>CLAVE C.T.</b></th>
					<th width="40"  style="border-bottom:1px solid #000;"><b>GRADO</b></th>
					<th width="70"  style="border-bottom:1px solid #000;"><b>PROMEDIO</b></th>
					<th width="60"  style="border-bottom:1px solid #000;"><b>RENAPO</b></th>
					<th width="60"  style="border-bottom:1px solid #000;"><b>SIIE</b></th>
					<th width="130" style="border-bottom:1px solid #000;"><b>MUNICIPIO</b></th>
				</tr>
			</table>';
		
		$this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html1, $border = 1, $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
	}

	// Page footer
	public function Footer() {
		// Position at 15 mm from bottom
		$this->SetY(-15);

		// Set font
		$this->SetFont('helvetica', '', 8);
		
		$fecha= date("d-m-Y");
		$hora=date('H:i:s');

		// Page number
		$this->Cell(0, 8, 'PAGINA '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'  '.$fecha.' '.$hora, 0, false, 'L', 0, '', 0, false, 'T', 'M');
	}

}


	// create new PDF document
	$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document ReporteParticipantesrmation
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Nicola Asuni');
	$pdf->SetTitle('Reporte');
	$pdf->SetSubject('Reporte');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

	// set default header data
	$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

	// set header and footer fonts
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
	}
	$pdf->SetMargins(2,23, 2);
	$pdf->AddPage('L', 'A4');

	$pdf->SetFont('helvetica', '', 7, '', true);

	$html = "";

	// ---------------------------------------------------------
 	for ($i = 0; $i < count($ReporteParticipantes); ++$i)  {
		$html .= '<table>';
			$html .= '<tr>';

				for ($j = 0; $j < count(array_keys($ReporteParticipantes[0])); ++$j) {
					$html .= '<th style="border-bottom:1px solid #000;" align="center">';
						$html .= $ReporteParticipantes[$i][ array_keys($ReporteParticipantes[0])[$j] ];
					$html .= '</th>';
				}

			$html .= 	'</tr>';
		$html .= '</table>';
	}

//$html .= '</table>';
$pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
//Close and output PDF document
//$pdf->Output('example_048.pdf', 'I'); // lo visualiza en una vista
//$mi_pdf = 'archivos/documento.pdf';
header('Content-type: application/pdf');
//header('Content-Disposition: attachment; filename="'.$pdf->Output('Reporte.pdf', 'I').'"');
readfile($pdf->Output('Reporte.pdf', 'I'));
//============================================================+
// END OF FILE
//============================================================+