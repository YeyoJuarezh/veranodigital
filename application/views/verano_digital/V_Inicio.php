<div id="Inicio" class="col-xs-10 noPadding">
	<div class="col-xs-3 noPadding">
		<ul>
			<li>
				<a href="#" id="btnModal1" data-toggle="modal" data-target=".inicioModal">
					<img class="imagenes navButtons" 
		 data-src="<?= base_url();?>assets/img/verano_digital/Inicio/Dirigido.png" 
	 data-hover="<?= base_url();?>assets/img/verano_digital/Inicio/Dirigido_hover.png" 
					src="<?= base_url();?>assets/img/verano_digital/Inicio/Dirigido.png" 
					alt='Botón para navegar a la sección "Dirigido a"'>
				</a>
			</li>
			<li>
				<a href="#" id="btnModal2" data-toggle="modal" data-target=".inicioModal">
					<img class="imagenes navButtons" 
		 data-src="<?= base_url();?>assets/img/verano_digital/Inicio/Proposito.png" 
	 data-hover="<?= base_url();?>assets/img/verano_digital/Inicio/Proposito_hover.png" 
					src="<?= base_url();?>assets/img/verano_digital/Inicio/Proposito.png" 
					alt='Botón para navegar a la sección "Propósito"'>
				</a>
			</li>
			<li>
				<a href="#" id="btnModal3" data-toggle="modal" data-target=".inicioModal">
					<img class="imagenes navButtons" 
		 data-src="<?= base_url();?>assets/img/verano_digital/Inicio/Modulos.png" 
	 data-hover="<?= base_url();?>assets/img/verano_digital/Inicio/Modulos_hover.png" 
					src="<?= base_url();?>assets/img/verano_digital/Inicio/Modulos.png" 
					alt='Botón para navegar a la sección "Módulos del curso"'>
				</a>
			</li>
		</ul>
	</div>

	<div class="col-xs-8 noPadding">
		<img id="logoInicio" class="col-xs-6 col-xs-offset-3 noPadding" src="<?= base_url();?>assets/img/verano_digital/logo.png" alt="Logotipo Verano Digi tam.">

		<img id="imgInicio" class="imagenes img-thumbnail" src="<?= base_url();?>assets/img/verano_digital/Inicio/fondoInicio.png" alt="Imagen central de la sección de Inicio.">
	</div>
</div>

<!-- inicioModal -->
<div class="modal fade bs-example-modal-lg inicioModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content modalContenido">
				<div class="modal-body">
						<img class="imagenes" id="imgModal1" src="<?= base_url();?>assets/img/verano_digital/botones_menu/modal/Dirigido.jpg" alt="top">
						<img class="imagenes" id="imgModal2" src="<?= base_url();?>assets/img/verano_digital/botones_menu/modal/Proposito.jpg" alt="top">
						<img class="imagenes" id="imgModal3" src="<?= base_url();?>assets/img/verano_digital/botones_menu/modal/Modulos.jpg" alt="top">
				</div>
		</div>
	</div>
</div>

