<div class="col-xs-9 col-xs-offset-1 noPadding">

		<div id="RegistroConcluido" class="col-xs-12 col-xs-offset-0 noPadding">
			<p id="text1">
				EL REGISTRO HA CONCLUIDO
			</p>
			<p id="text2">
				¡GRACIAS POR TU PARTICIPACIÓN!
			</p>
		</div>

	<div id="Registro" class="col-xs-12 noPadding">
		<img src="<?= base_url();?>assets/img/verano_digital/Registro/registro.png" alt="top">
		<div class="col-xs-8">
				<form name="form_registro">
					<div class="row">

							<div class="col-xs-12">
								<div class="form-group ">
									<input type="text" class="form-control form_input" id="nombre" placeholder="NOMBRE (S)" maxlength="200">
								</div>
							</div>


							<div class="col-xs-6">
								<div class="form-group">
									<input type="text" class="form-control form_input" id="ap_pa" placeholder="APELLIDO PATERNO" maxlength="100">
								</div>
							</div>
							<div class="col-xs-6 noPaddingL">
								<div class="form-group">
									<input type="text" class="form-control form_input" id="ap_ma" placeholder="APELLIDO MATERNO" maxlength="100">
								</div>
							</div>


							<div class="col-xs-3 noPaddingR">
								<div class="form-group">
									<select class="form-control form_input" id="idCatGenero">
										<option value="0" disabled selected>GENERO</option>
										<?= $sexos; ?>
									</select>
								</div>
							</div>
							<div class="col-xs-3">
								<div class="form-group">
									<select class="form-control form_input" id="idCatMunicipio">
										<option value="0" disabled selected>MUNICIPIO</option>
										<?= $municipios; ?>
									</select>
								</div>
							</div>
							<div class="col-xs-4 noPadding">
								<div class="form-group">
									<select class="form-control form_input" id="idCatPoblado">
										<option value="0" disabled selected>LOCALIDAD</option>
										<?= $poblados; ?>
									</select>
								</div>
							</div>
							<div class="col-xs-2 noPadding">
								<input id="btnOtroPoblado" type="button" class="btn btn-primary" value="OTRA" data-toggle="modal" data-target=".modalOtros">
							</div>

							<div class="col-xs-8 noPaddingR">
								<div class="form-group" data-toggle="tooltip" data-placement="left" title="SELECCIONA EL NOMBRE OFICIAL DE TU ESCUELA">
									<select class="form-control form_input selectpicker" data-show-subtext="true" data-live-search="true" id="idCatEscuela"> 
									<!-- <select class="form-control form_input" id="idCatEscuela"> -->

										<option value="0" disabled selected>NOMBRE OFICIAL DE ESCUELA DE PROCEDENCIA</option>
										<option value="1">- OTRA ESCUELA -</option>'
										<?= $escuelas; ?>
									</select>
								</div>
							</div>

							<div class="col-xs-4">
								<div class="form-group">
									<input type="text" class="form-control form_input" id="grado" placeholder="GRADO" maxlength="2">
								</div>
							</div>

						</div>
						<div class="row">

							<div class="col-xs-4 noPaddingR">
								<div class="form-group">
									<input type="email" class="form-control form_input" id="email" placeholder="EMAIL" maxlength="40">
								</div>
							</div>

							<div class="col-xs-4 noPaddingR">
								<div class="form-group">
									<input type="email" class="form-control form_input" id="email_tutor" placeholder="EMAIL PADRE / TUTOR" maxlength="40">
								</div>
							</div>



							<div class="col-xs-4">
								<div class="form-group">
									<input type="text" class="form-control form_input" id="telefono" placeholder="TELÉFONO" maxlength="15">
								</div>
							</div>
							<div class="col-xs-8 noPaddingR">
								<div class="form-group">
										<div class="col-xs-6 noPadding">
											<input type="date" class="form-control form_input" id="fecha_nac">
										</div>
										<label id="labelFecha" for="fecha_nac" class="col-xs-6 control-label noPadding">FECHA DE NACIMIENTO</label>
								</div>
							</div>

					</div>
				</form>
				<br>
				<input id="registrarParticipante" type="button" class="btn btn-primary" value="REGISTRAR">
				<input id="limpiar" type="button" class="btn" value="LIMPIAR">
				<!--
				<input id="BotonDePrueba" type="button" class="btn btn-primary" value="Botón de Prueba">
				-->

				
				<button id="btn11">
					<i class="fa fa-check"></i>
				</button>
				<button id="btn22">
					<i class="fa fa-times"></i>
				</button>
				<button id="btn33">
					<i class="fa fa-exclamation-triangle"></i>
				</button>
				
		</div>
	</div>
</div>

<!-- Toast -->
<div id="alert" class="hide"></div>

<!-- Modal Otras Localidades -->
<div class="modal fade modalOtros" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Escribe el nombre de tu licalidad si ésta <b>NO</b> aparece en el listado anterior.</h4>
			</div>
			<div class="modal-body">

					<!--
					<div class="col-xs-12">
						<div class="form-group">
							<select class="form-control form_input selectpicker" data-show-subtext="true" data-live-search="true" id="idCatMunicipio">
								<option value="0" disabled selected>MUNICIPIO</option>
								<?= $municipios; ?>
							</select>
						</div>
					</div>
					-->
					<div class="col-xs-12">
						<div class="form-group">
								<input type="text" class="form-control form_input" id="nuevoPoblado" placeholder="POBLADO" maxlength="100">
								<!--
								<input type="text" class="form-control form_input" id="nuevaEscuela" placeholder="ESCUELA DE PROCEDENCIA" maxlength="100">
								-->
						</div>
					</div>

			</div>
			<div class="modal-footer">
				<button type="button" id="guardarOtro" class="btn btn-primary">Guardar</button>
			</div>
		</div>
	</div>
</div>



<!-- modalGenerico -->
<div class="modal fade bs-example-modal-lg" id="modalGenerico" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"></h4>
				</div>
				<div class="modal-body">
						<div class="row">
							<div class="col-xs-2 ImagenModal noPadding">
								<i id="IconoModal" class="fa fa-check"></i>
							</div>
							<div class="col-xs-10 TextoModal">
								<p id="TextoModalTitulo">--</p>
								<p id="TextoModalContenido">--</p>
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<br class="modalSinFooter">
					<button type="button" class="btn btn-default modalConFooter" data-dismiss="modal">CANCELAR</button>
					<input id="continuarRegistro" type="button" class="btn btn-primary modalConFooter" value="REGISTRAR">
				</div>

		</div>
	</div>
</div>