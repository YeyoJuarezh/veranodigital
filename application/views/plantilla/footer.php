<div id="footer">
	<div id="footer-bg">
		<div class="container">
			<div id="footer-rows" class="row cols-same-height">
				<div class="info col-md-2 hidden-sm hidden-xs">
					<h2>Secretaría de Educación</h2>
          <p>
	          Calzada General Luis Caballero S/N,<br>Fracc. Las Flores, C.P. 87078,<br>Cd. Victoria, Tamaulipas, México.
					</p>
					<p>
						01 834 318 6600<br>01 834 318 6601<br>01 834 318 6602<br>
					</p>
				</div>
				<div id="footer-no-diagonal" class="col-md-1 hidden-sm hidden-xs"></div>
				<div id="footer-links" class="col-md-9 hidden-sm hidden-xs">
				</div>
			</div>
		</div>

		<div class="container-fluid">
			<div class="row">
				<div class="info col-sm-12 visible-sm-block visible-xs-block">
					<h2>Secretaría de Educación</h2>
          <p>
          	Calzada General Luis Caballero S/N,<br>Fracc. Las Flores, C.P. 87078,<br>Cd. Victoria, Tamaulipas, México.
					</p>
					<p>
						01 834 318 6600<br>01 834 318 6601<br>01 834 318 6602<br>
					</p>
				</div>
			</div>
		</div>

	</div>
</div>
<!-- /Footer --><!-- Liston inferior -->
<div id="liston-inferior">
  Todos los derechos reservados © 2016
  <span id="to-break">/</span>
  Gobierno del Estado de Tamaulipas 2016 - 2022
</div>


<!-- /Liston inferior -->
<script type="text/javascript">
jQuery(function($) {
	$(".wpcf7-submit").click(function(event) {
		var messageOutput = $(this).closest("form").children(".wpcf7-response-output");
		$(document).ajaxComplete(function() {
			var message = $(messageOutput).html();
			var validMessage = function(){
				swal({
					type: "success",
					title: "",
					text: message,
					timer: 3000,
					showConfirmButton: false
				});
			};
			var errorMessage = function(){
				swal({
					type: "warning",
					title: "",
					text: message,
					timer: 3000,
					showConfirmButton: false
				});
			};
			setSwal = $(".wpcf7-response-output").hasClass("wpcf7-validation-errors") ? "alert" : "success";
			if ( setSwal === "alert" ) { errorMessage() };
			if ( setSwal === "success" ) { validMessage() };
		});
	});
    
});
</script>
<script type="text/javascript" src="<?=base_url()?>assets/js/handlebars.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/purl.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.fancybox.pack.js(1)"></script>
<script type="text/javascript">
/* <![CDATA[ */
var media_superior_revoe = {"ajax_url":"http:\/\/www.tamaulipas.gob.mx\/educacion\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type="text/javascript" src="<?=base_url()?>assets/js/media-revoe-cliente.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var superior_revoe = {"ajax_url":"http:\/\/www.tamaulipas.gob.mx\/educacion\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type="text/javascript" src="<?=base_url()?>assets/js/sup-revoe-cliente.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var becas = {"ajax_url":"http:\/\/www.tamaulipas.gob.mx\/educacion\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type="text/javascript" src="<?=base_url()?>assets/js/becas-client.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var directorio = {"ajax_url":"http:\/\/www.tamaulipas.gob.mx\/educacion\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type="text/javascript" src="<?=base_url()?>assets/js/directorio-client.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var forte = {"ajax_url":"http:\/\/www.tamaulipas.gob.mx\/educacion\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type="text/javascript" src="<?=base_url()?>assets/js/forte-client.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.form.min.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var _wpcf7 = {"loaderUrl":"http:\/\/www.tamaulipas.gob.mx\/educacion\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","recaptcha":{"messages":{"empty":"Por favor, prueba que no eres un robot."}},"sending":"Enviando..."};
/* ]]> */
</script>
<script type="text/javascript" src="<?=base_url()?>assets/js/scripts.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/ssba.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/wp-embed.min.js"></script>
<script>update_all_hights();</script>

<!--     PROYECTO -->
<script type="text/javascript" src="<?=base_url()?>assets/js/verano_digital/bootstrap-select.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/verano_digital/toastr.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/verano_digital/ajaxFunction.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/verano_digital/validaciones.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/verano_digital/verano_digital.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/js/verano_digital/Galeria.js"></script>
<!-- end PROYECTO -->

<div id="fb-root" class=" fb_reset">
	<div style="position: absolute; top: -10000px; height: 0px; width: 0px;">
		<div></div>
	</div>
	<div style="position: absolute; top: -10000px; height: 0px; width: 0px;">
		<div></div>
	</div>
</div>
</body>
</html>