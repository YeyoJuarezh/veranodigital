<div class="contenedor-principal">
	<div class="container">

		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<div class="form-login">
					<center><img src="<?=base_url();?>assets/img/user.jpg" alt="..." class="img-rounded" width="110" height="130"></center>
					 
						<br>

					<form action="<?php echo base_url();?>" method="post" role="form" class="formValidate">
						<div class="col-lg-12 input-group">
							<span class="input-group-addon" id="basic-addon1"><i class="fa fa-user"></i></span>
								<input type="text" name="usuario" class="form-control required" id="usuario_id" data-placement="left" placeholder="Usuario/Correo" aria-describedby="bassic-addon1">
						</div>

							<br>

						<div class="col-lg-12 input-group">
							<span class="input-group-addon" id="basic-addon1"><i class="fa fa-unlock-alt"></i></span>
								<input type="password" name="password" class="form-control required" id="password_id" data-placement="left" placeholder="Contraseña" aria-describedby="bassic-addon1">
						</div>

							<br>

						<?php if ($this->session->flashdata('mensaje')): ?>
							<p class="alert alert-warning" style="font-size: 13px;" role="alert">
								<span class="glyphicon glyphicon-exclamation-sign" style="font-size: 15px; color: red;"></span>&nbsp;<?php echo $this->session->flashdata('mensaje'); ?>
							</p>
						<?php endif ?>

						<div class="col-lg-12 input-group">
							<input type="submit" class="btn btn-primary btn-xm btn-block" value="Entrar">
							<!-- <a href="<?=base_url();?>C_Registro" class="btn btn-info btn-xm btn-block">Registrarse como Responsable del Sistema</a> -->
						</div>

							<br>

						<div class="col-lg-12 input-group">
							<a href="<?=base_url()?>" class="btn btn-success btn-xm btn-block">¿Olvide mi contraseña?</a>
						</div>

							<br>

						<script>
							$(document).ready(function() {
								$('.formValidate').validate({
									errorElement: 'span',
								})
							})
						</script>

					</form>

						<br>

				</div>
				<div class="col-md-4"></div>
			</div>
		</div>
	</div>
</div>