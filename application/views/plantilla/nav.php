<div id="menu-secretarias">
	<input type="checkbox" name="menu-toggle" id="menu-open">
  <div class="container">
		<div id="row-cabecera" class="row">
		
			<!-- Menú para escritorio -->
			<div class="menu-escritorio col-xs-12 hidden-xs hidden-sm">
				<div id="menu-container" class="menu-menu-principal-container">
				</div>
			</div>
			<!-- Menú para escritorio -->
		
			<div id="barras-boton" class="col-xs-offset-6 col-xs-6 visible-xs-block visible-sm-block">
				<div class="menu-btn-container">
					<label for="menu-open" id="menu-btn" class="btn-movil">
						<div class="menu-bars">
							<span></span>
							<span></span>
							<span></span>
						</div>
					</label>
				</div>
			</div>
		</div>
  </div>
  
  <div id="row-menu-movil" class="container hidden-md hidden-lg">
		<div class="menu-menu-principal-container">
			<ul id="menu-menu-principal-1" class="clean-list menu-movil"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-285"><a href="http://www.tamaulipas.gob.mx/educacion/">Inicio</a></li>
				<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-551"><a href="http://www.tamaulipas.gob.mx/educacion/convocatoria/comunicado-de-tutoria-en-linea/#">Secretaría<br>de Educación</a>
					<ul class="sub-menu">
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-372"><a href="http://www.tamaulipas.gob.mx/educacion/secretaria-de-educacion/conocenos/">Conócenos</a></li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-367"><a href="http://www.tamaulipas.gob.mx/educacion/secretaria-de-educacion/codigo-de-etica/">Código de ética</a></li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-790"><a href="http://www.tamaulipas.gob.mx/educacion/secretaria-de-educacion/directorio/">Directorio de funcionarios</a></li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-366"><a href="http://www.tamaulipas.gob.mx/educacion/secretaria-de-educacion/calendario-escolar/">Calendario escolar</a></li>
					</ul>
				</li>
				<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-552"><a href="http://www.tamaulipas.gob.mx/educacion/convocatoria/comunicado-de-tutoria-en-linea/#">Información<br>por Perfiles</a>
					<ul class="sub-menu">
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-462"><a href="http://www.tamaulipas.gob.mx/educacion/perfiles/docentes/">Docentes</a>
							<ul class="sub-menu">
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-464"><a href="http://www.tamaulipas.gob.mx/educacion/perfiles/docentes/programas/">Programas para Docentes</a></li>
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-463"><a href="http://www.tamaulipas.gob.mx/educacion/perfiles/docentes/basica/">Educación Básica</a></li>
							</ul>
						</li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-471"><a href="http://www.tamaulipas.gob.mx/educacion/perfiles/escuelas/">Escuelas</a>
							<ul class="sub-menu">
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-475"><a href="http://www.tamaulipas.gob.mx/educacion/perfiles/escuelas/programa/">Programas Generales</a></li>
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-477"><a href="http://www.tamaulipas.gob.mx/educacion/perfiles/escuelas/programas-basica/">Programas para la Educación Básica</a></li>
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-481"><a href="http://www.tamaulipas.gob.mx/educacion/perfiles/escuelas/programas-media-superior-y-superior/">Programas para la Educación Media Superior y Superior</a></li>
							</ul>
						</li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-460"><a href="http://www.tamaulipas.gob.mx/educacion/perfiles/estudiante/">Estudiante</a>
							<ul class="sub-menu">
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-466"><a href="http://www.tamaulipas.gob.mx/educacion/perfiles/estudiante/becas/">Becas</a></li>
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-465"><a href="http://www.tamaulipas.gob.mx/educacion/perfiles/estudiante/directorio-bibliotecas/">Directorio de Bibliotecas</a></li>
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-467"><a href="http://www.tamaulipas.gob.mx/educacion/perfiles/estudiante/centros-de-educacion-extraescolar/">Centros de Educación Extraescolar</a></li>
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-461"><a href="http://www.tamaulipas.gob.mx/educacion/perfiles/estudiante/media-superior-y-superior/">Educación Media Superior y Superior</a></li>
							</ul>
						</li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-492"><a href="http://www.tamaulipas.gob.mx/educacion/perfiles/padre-de-familia/">Padre de Familia</a>
							<ul class="sub-menu">
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-495"><a href="http://www.tamaulipas.gob.mx/educacion/perfiles/estudiante/becas/">Becas</a></li>
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-496"><a href="http://www.tamaulipas.gob.mx/educacion/perfiles/estudiante/directorio-bibliotecas/">Directorio de Bibliotecas</a></li>
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-494"><a href="http://www.tamaulipas.gob.mx/educacion/perfiles/padre-de-familia/basica/">Educación Básica</a></li>
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-493"><a href="http://www.tamaulipas.gob.mx/educacion/perfiles/padre-de-familia/media-superior-y-superior/">Educación Media Superior y Superior</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-554"><a href="http://www.tamaulipas.gob.mx/educacion/convocatoria/comunicado-de-tutoria-en-linea/#">Información<br>por Áreas</a>
					<ul class="sub-menu">
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-379"><a href="http://www.tamaulipas.gob.mx/educacion/areas/administracion/">Subsecretaría de Administración</a></li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-378"><a href="http://www.tamaulipas.gob.mx/educacion/areas/basica/">Subsecretaría de Educación Básica</a></li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-377"><a href="http://www.tamaulipas.gob.mx/educacion/areas/media-superior-y-superior/">Subsecretaría de Educación Media Superior y Superior</a></li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-376"><a href="http://www.tamaulipas.gob.mx/educacion/areas/planeacion/">Subsecretaría de Planeación</a></li>
						<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-380"><a href="http://www.tamaulipas.gob.mx/educacion/areas/unidad-ejecutiva/">Unidad Ejecutiva</a></li>
					</ul>
				</li>
				<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-563"><a href="http://www.tamaulipas.gob.mx/educacion/convocatorias/">Convocatorias</a></li>
				<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-362"><a href="http://www.tamaulipas.gob.mx/educacion/servicios-en-linea/">Servicios<br>en Línea</a></li>
				<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-573"><a href="http://www.tamaulipas.gob.mx/educacion/prensa/">Sala de<br> Prensa</a></li>
				<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1106"><a target="_blank" href="http://transparencia.tamaulipas.gob.mx/">Transparencia</a></li>
			</ul>
		</div>
	</div>
</div>

<!-- Menú --><!-- Breadcrumbs -->
<div id="breadcrumbs">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="bcn"></div>
			</div>
		</div>
	</div>
</div>