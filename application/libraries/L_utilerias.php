<?php
//include 'PHPMailer-master/class.phpmailer.php';
class L_utilerias {

	private $CI;

    private $mensaje;
    private $dataRelationTable = array();
    private $arreglo = array();
    private $alfaveto = array();
    private $prueba_array2 = array();
    private $a = 65;
    private $b = 65;
    private $limite = 90;


	function __construct() {

		$this->CI = get_instance();


	}

    public function relationTable($tabla, $campo) {

        $this->dataRelationTable['rvoe_regusuarios']['estado'] = array('tabla' => 'rvoe_catestados', 'join' => 'a.estado = estado.id');
        $this->dataRelationTable['rvoe_regusuarios']['municipio'] = array('tabla' => 'rvoe_catmunicipios', 'join' => 'a.municipio = municipio.id');
        $this->dataRelationTable['rvoe_regusuarios']['rol'] = array('tabla' => 'rvoe_catroles', 'join' => 'a.rol = rol.id');
        $this->dataRelationTable['rvoe_regusuarios']['nivel_id'] = array('tabla' => 'rvoe_catniveleducativo', 'join' => 'a.nivel_id = nivel.id');

    }

	public function utilQuitaAcentos($string) {

		$string = str_replace(array('á','à','â','ã','ª','ä'),"a",$string);
    	$string = str_replace(array('Á','À','Â','Ã','Ä'),"A",$string);
    	$string = str_replace(array('Í','Ì','Î','Ï'),"I",$string);
    	$string = str_replace(array('í','ì','î','ï'),"i",$string);
    	$string = str_replace(array('é','è','ê','ë'),"e",$string);
    	$string = str_replace(array('É','È','Ê','Ë'),"E",$string);
    	$string = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$string);
    	$string = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$string);
    	$string = str_replace(array('ú','ù','û','ü'),"u",$string);
    	$string = str_replace(array('Ú','Ù','Û','Ü'),"U",$string);
    	$string = str_replace(array('[','^','´','`','¨','~',']'),"",$string);
    	$string = str_replace("ç","c",$string);
    	$string = str_replace("Ç","C",$string);
    	$string = str_replace("ñ","n",$string);
    	$string = str_replace("Ñ","N",$string);
    	$string = str_replace("Ý","Y",$string);
    	$string = str_replace("ý","y",$string);

    	$string = str_replace("&aacute;","a",$string);
    	$string = str_replace("&Aacute;","A",$string);
    	$string = str_replace("&eacute;","e",$string);
    	$string = str_replace("&Eacute;","E",$string);
    	$string = str_replace("&iacute;","i",$string);
    	$string = str_replace("&Iacute;","I",$string);
    	$string = str_replace("&oacute;","o",$string);
    	$string = str_replace("&Oacute;","O",$string);
    	$string = str_replace("&uacute;","u",$string);
    	$string = str_replace("&Uacute;","U",$string);

    	return $string;

	}

    public function randomString($length) {

        $characters = '0123456789_.abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {

            $randomString .= $characters[rand(0, $charactersLength - 1)];

        }

        return $randomString;

    }

    public function phpmail($usuario, $password) {

        require_once('PHPMailer-master/class.phpmailer.php');
        require_once('PHPMailer-master/class.smtp.php');

        $msj = '<html>
                    <head>
                        <title>Notificación de Sistema RVOE</title>
                        <link href="bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
                    </head>
                    <body>
                        <div style="width: 95%; display: block;">
                            <div style="width: 75%; margin: 0 auto;"><img src="../../assets/img/CETE.jpg" width=55 height=50></img></div>
                                <div style="width: 75%; margin: 0 auto; border-radius: 5px 5px 0 0; background-color: #E4DEDE; padding-top: 2em; padding-left: 2em; display: block; border: 1px #E4DEDE solid;">
                                    <h2>Notificación de registro a sistema RVOE</h2>
                                </div>
                                <div style="width: 75%; margin: 0 auto; background: #FFF; padding-top: 2em; padding-left: 2em; border: 1px #E4DEDE solid; display: block;">
                                    <div class=\"headersPresupuesto\" style="color:#000000;">Datos del Servicio:</div><br />
                                    <div class=\"headersPresupuesto\" style="color:#000000;">Usuario: ' . $usuario .' </div>
                                    <div class=\"headersPresupuesto\" style="color:#000000;">Contraseña: ' . $password .'</div>
                        <br />
        </div></div></div></body></html>';

        return $msj;

    }

    public function getRegistros($parametro = null) {

        if ($parametro) {

            $this->registros = $parametro;

        }else{

            return $this->registros;
        }


    }

    public function creaArray($data) {

        $array = explode(',',$data);

        return $array;
    }

    public function columnasExcel($data) {


        foreach ($data as $key => $value) {

            $indice = $this->convierteChart($key);

            $this->alfaveto[$indice] = $value;

        }


        return $this->alfaveto;

    }

    public function convierteChart($indice) {

        $variable = $this->a+$indice;
        if ($variable <= $this->limite) {

            $indice = chr($variable);

        }else if($variable >= $this->limite) {

            $indice = chr($this->a) . (chr($variable-25));

        }




        return $indice;

    }
    
    public function scapeString($valor) {
    
      $valor= str_replace("<script>",  "", $valor);
      $valor= str_replace("</script>", "", $valor);
      $valor= str_replace("\"",        "", $valor);
      $valor= str_replace("<?",        "", $valor);
      $valor= str_replace("?>",        "", $valor);
      
      $valor= htmlspecialchars(addslashes($valor));
      
      return $valor;
      
    }

    public function downloadZip($filename, $temporal) {

        header("Content-type: application/zip");

        header("Content-Disposition: attachment; filename=$filename.zip");

        header("Cache-Control: no-cache, must-revalidate");

        header("Expires: 0");

        readfile("$temporal.zip");

        unlink("$temporal.zip");

    }



}
 ?>