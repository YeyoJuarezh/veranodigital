<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verano_digital
{
	public function	construirSelect($data_set){
		//print_r( $data_set);
		$select_armado = '';

		if ( count($data_set) > 0 ) {
			foreach ($data_set as $valores):
				$select_armado .=	'<option ';
				if (count($valores) == 3) {
					$select_armado .=	'data-subtext="'.$valores['Subtext'].'" ';
				}
				$select_armado .=	'value="'.$valores['Id'].'">';
				$select_armado .=	 $valores['Elemento'];
				$select_armado .=	'</option>';
			endforeach;
		}else{
			$select_armado .=	'<option value="'. 0 .'" disabled selected>';
			$select_armado .=	'- Sin información -';
			$select_armado .=	'</option>';
		}
		//echo $select_armado;
		return $select_armado;
	}

}

/* End of file verano_digital.php */
/* Location: ./application/libraries/verano_digital.php */
