<?php 
class L_tabla {

	private $CI;

	private $tablaOpen = '<table class="';
	private $tablaEnd;
	private $tablaHeader;
	private $tableBody;
	private $creaOpciones;
	public  $url;

	function __construct() {

		$this->CI = get_instance();

	}

	public function tablaOpen($class) {

		$this->tablaOpen .= $class.'">';

		return $this->tablaOpen;

	}

	public function tablaHeader($data) {

		$this->tablaHeader .= '<tr>';

		foreach ($data as $indice => $valor) {
		
			$this->tablaHeader .= '<th>'.$valor.'</th>';	

		}

		$this->tablaHeader .= '</tr>';

		return $this->tablaHeader;

	}

	public function tableBody($data, $opciones = null, $valores) {

		$this->tableBody .= '<tr>';

			foreach ($data as $indice => $valor) {

				$this->tableBody .= '<td>';

				foreach ($opciones as $indice_opciones => $valor_opciones) {
					
					$this->tableBody .= $this->creaOpciones($indice_opciones, $valor_opciones);
					
				}

				$this->tableBody .= '</td>';
				

				foreach ($valores as $indice_valores => $valor_valores) {
					
					$this->tableBody .= '<td>' . $valor[$valor_valores] . '</td>';

				}
				
				
			}

		$this->tableBody .= '</tr>';

		return $this->tableBody;

	}

	public function tablaUrl($url) {

		$this->url = $url;

	}

	public function creaOpciones($valor, $clase) {

		$this->creaOpciones = '<a href="' . $this->url . $valor . ' " class="btn ' . $clase . '">' . $valor . '</a> ';

		return $this->creaOpciones;

	}

	public function tablaEnd() {

		$this->tablaEnd = '</table>';

		return $this->tablaEnd;
	}


}
 ?>