<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_main extends CI_Model {

  function __construct() {
    parent::__construct();

    $this->load->database();
  }

  public function login($usuario, $password) {

    $this->db->select('id, nombre, apellido_p, apellido_m, rol_id');
    $this->db->from('infor_usuario');
    $this->db->where('correo', $usuario);
    $consulta = $this->db->get();

    if ($consulta->num_rows() > 0) {

      $this->db->where('correo', $usuario)->where('password', $password)->where('alta_baja', 1);
      $this->db->from('infor_usuario');
      $consulta1 = $this->db->get();

      if ($consulta1->num_rows() > 0) {

        $consulta = $consulta->row();
        $data     = array(
                'id'      => $consulta->id,
                'nombre'    => $consulta->nombre.' '.$consulta->apellido_p.' '.$consulta->apellido_m,
                'rol'     => $consulta->rol_id,
                'ct' => '',
                'registros' => 10
              );
        $this->session->set_userdata($data);

      } else {
        $this->session->set_flashdata('mensaje', 'Contraseña Incorrecta, Verifique!');
      }

    } else {
      $this->session->set_flashdata('mensaje', 'Usuario Incorrecto, Verifique!');
    }

  }
}
 ?>