<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_querys extends CI_Model {

  function __construct() {
    parent::__construct();

    $this->load->database();

  }

  #Estructura de metodo consulta()
  # $this->M_query->consulta(SELECT[campos a seleccionar], FROM[primera tabla a seleccionar], JOIN[uniones de tablas van dentro de un array ej: array(array('tabla' => 'tabla a', 'join' => 'a.campo_id = b.id'), array('tabla' => 'tabla c', 'join' => 'a.campo_id = c.id'))], WHERE[Condicion(es)], START[inicio de rango de valores], LIMIT[final de rango de valores], ORDER[campo a ordenar], GROUP BY[campo a agrupar])

  public function consulta($select, $tabla, $join = null, $where = null, $start = null , $limit = null, $order = null, $group = null, $ordenamiento = null) {

    $this->db->select($select);

    $this->db->from($tabla);

    if ($where) { $this->db->where($where);}

    if (is_array($join)) {

      for ($i=0; $i < sizeof($join) ; $i++) {

        if (isset($join[$i]['tabla'])) {

          $this->db->join($join[$i]['tabla'], $join[$i]['join'], 'left');

        }

      }

    }

    if ($limit != 0 || $limit != null) {
      $this->db->limit($limit, $start);
    }

    if ($group) {

      $this->db->group_by("$group");

    }

    if ($order) {

      $this->db->order_by($order, "$ordenamiento");

    }

    $consulta = $this->db->get();
    $resultado = $consulta->result_array();

    return $resultado;

  }


  function getRowCount($st = NULL, $tabla, $campo)  {

    if ($st == "NIL");
    $this->db->select('*');
    $this->db->from($tabla);
    $this->db->like($campo, $st);
    //$sql = "select * from rvoe_regplanteles where nombre_plantel like '%$st%'";
    $resultado = $this->db->get();
    return $resultado->num_rows();

  }

  public function getData($select, $tabla, $join = null,$where = null, $limit, $start, $st = NULL, $like, $order = null, $group = null, $ordenamiento = null) {

    $this->db->select($select);
    $this->db->from($tabla);

    if ($where) { $this->db->where($where);}

    if (is_array($join)) {

      for ($i=0; $i < sizeof($join) ; $i++) {

        if (isset($join[$i]['tabla'])) {

          $this->db->join($join[$i]['tabla'], $join[$i]['join']);

        }

      }

    }

    if ($like) {

      $this->db->like($like,$st);

    }

    if ($limit != 0 || $limit != null) {

      $this->db->limit($limit, $start);

    }

    if ($order && ($order != 'id' || $order != 'a.id')) {

      $this->db->order_by($order, "$ordenamiento");

    } else {

      $this->db->order_by('id', "$ordenamiento");

    }

    if($group) {

      $this->db->group_by($group);

    }

    $consulta = $this->db->get();
     $resultado = $consulta->result_array();

    return $resultado;

  }
  public function getData2($select, $tabla, $join = null,$where = null, $limit, $start, $st = NULL, $like, $order = null) {

    $this->db->select($select);
    $this->db->from($tabla);

    if ($where) { $this->db->where($where);}

    if (is_array($join)) {

      for ($i=0; $i < sizeof($join) ; $i++) {

        if (isset($join[$i]['tabla'])) {

          $this->db->join($join[$i]['tabla'], $join[$i]['join'], $join[$i]['tipo']);

        }

      }

    }

    if ($like) {

      $this->db->like($like,$st);

    }

    if ($limit != 0 || $limit != null) {

      $this->db->limit($limit, $start);

    }

    if ($order && $order != 'id') {

      $this->db->order_by($order, "asc");

    } else {

      $this->db->order_by('id', 'desc');

    }

    $consulta = $this->db->get();
     $resultado = $consulta->result_array();

    return $resultado;

  }

  public function guardar($tabla, $data, $id = null) {

    if ($id) {

      $this->db->where("id", $id);
      $this->db->update($tabla, $data);

      $this->session->set_flashdata('mensaje', 'Registro guardado correctamente');

    } else {

      $this->db->insert($tabla, $data);
      $id_insert = $this->db->insert_id();

      if ($id_insert) {

        $this->session->set_flashdata('mensaje', 'Registro guardado correctamente');

      }else{

        $this->session->set_flashdata('mensaje', 'Error al guardar registro');

      }

      return $id_insert;

    }

  }

  public function actualiza($tabla, $data, $where) {

    $this->db->where($where);
    $this->db->update($tabla, $data);

  }

  public function eliminar($campo, $tabla, $id) {
    $this->db->where($campo, $id);
    $this->db->delete($tabla);
  }
  public function getpdf2($array){
    $op1=$array['rango1'];
    $op2=$array['rango2'];
    $opciones=$array['opciones'];
    $todos=$array['todos'];
    $dat_imp=$array['dat_imp'];
    $tablas = $array['tablas'];

    $this->db->select('*');
    $this->db->from($tablas[$dat_imp]['tabla']);
    if ($array['var'] == 1) {
      if ($opciones == 1) {
        if (empty($op1) && empty($op2)) {
          $this->db->where($tablas[$dat_imp]['nombre'], $op1);
        }elseif (empty($op2)) {
          $this->db->like($tablas[$dat_imp]['nombre'], $op1);
        }else{
          $this->db->like($tablas[$dat_imp]['nombre'], $op1,'after');
          $this->db->or_like($tablas[$dat_imp]['nombre'], $op2,'after');
        }
      }elseif ($opciones == 2) {
        $this->db->order_by($tablas[$dat_imp]['nombre'], 'ASC');
      }
    }else if($array['var'] == 2){
      if ($opciones == 1) {
        $this->db->where($tablas[$dat_imp]['id'].' >=', $op1);
        $this->db->where($tablas[$dat_imp]['id'].' <=', $op2);
      }elseif ($opciones == 2) {
        $this->db->order_by($tablas[$dat_imp]['id'], 'ASC');
      }
    }
    $q = $this->db->get();
        return $q->result();
        $q->free_result();
  }
  public function imprimir($registros, $tabla, $joins = null) {
    $qry='SELECT '.$registros.' FROM '.$tabla.' '.$joins;
    $result=$this->db->query($qry);
    return $result->result_array();
  }
  public function guardarfecha($data, $tabla, $id = null) {

    if ($id) {
      $this->db->where('id', $id);
      $this->db->update($tabla, $data);

      $id_insert = $id;

    }else{

      if (array_key_exists("0", $data)) {
        for ($i=0; $i < sizeof($data) ; $i++) {

          $this->db->insert($tabla, $data[$i]);

        }

      }else{

        $valida_fecha = $this->valida_fecha($data['fecha']);
        if ($valida_fecha == true) {

          $this->session->set_flashdata('mensaje',"El año ya se encuentra registrado.");

        }else{

          $this->db->insert($tabla, $data);

        }

      }


      $id_insert = $this->db->insert_id();
      if ($id_insert) {


        $this->session->set_flashdata('mensaje', 'Registro agregado correctamente');    #Si recivimos el id que se creo creamos un mensaje para el usuario
      }

      return $id_insert;
    }


  }

  public function valida_fecha($fecha) {

    $this->db->select('fecha');
    $this->db->from('rvoe_catfechafiscal');
    $this->db->where('fecha', $fecha);

    $consulta = $this->db->get();
    if($consulta->num_rows() > 0) {

      $correo = true;

    }else{

      $correo = false;

    }

    return $correo;

  }
}
 ?>