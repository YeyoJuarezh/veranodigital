<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Reportes extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	public function ObtenerReporteParticipantes(){
		$this->db->select("
			part.Id, 
			part.Nombre, 
			part.Ap_pa, 
			part.Ap_ma, 
			cel_sex.Elemento AS Genero,
			cel_mun.Elemento as Municipio,
			clo.Elemento as Localidad,
			cel_mun2.Elemento as nuevoMunicipio,
			part.nuevoPoblado, 
			cse.Elemento AS Escuela,
			part.Grado, 
			part.Email, 
			part.Email_tutor, 
			part.Telefono, 
			part.Fecha_nac AS 'Fecha de nacimiento', 
			part.FR AS 'Fecha y hora de registro'
		");
		$this->db->from('Participantes AS part');
		$this->db->join('CatElementos AS cel_sex',	'cel_sex.Id = part.IdCatGenero',			'inner');
		$this->db->join('CatLocalidades AS clo',		'clo.Id = part.IdCatPoblado',					'inner');
		$this->db->join('CatElementos AS cel_mun',	'cel_mun.Id = clo.IdCatMunicipio',		'left');
		$this->db->join('CatElementos AS cel_mun2',	'cel_mun2.Id = part.IdCatMunicipio',	'left');
		$this->db->join('CatSecundarias AS cse',		'cse.Id = part.IdCatEscuela',					'inner');
		return $this->db->get()->result_array();
	}
}

/* End of file M_Reportes.php */
/* Location: ./application/models/verano_digital/M_Reportes.php */