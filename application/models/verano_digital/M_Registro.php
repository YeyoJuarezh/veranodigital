<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Registro extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	public function ObtenerSexo(){
		$this->db->select("Id, Elemento");
		$this->db->from('CatElementos');
		$this->db->where('Catalogo', 'CatSexo');
		return $this->db->get()->result_array();
	}

	public function ObtenerMunicipios(){
		$this->db->select("Id, Elemento");
		$this->db->from('CatElementos');
		$this->db->where('Catalogo', 'CatMunicipio');
		return $this->db->get()->result_array();
	}

	public function ObtenerLocalidades($registroInfo = array('IdMunicipio' => 0)){
		$this->db->select("Id, Elemento");
		$this->db->from('CatLocalidades');
			$this->db->where('IdCatMunicipio', $registroInfo['IdMunicipio']);
			$this->db->not_like('Elemento', 'Ninguno', 'after');
			//$this->db->or_where('catl.Id', 1);
		$this->db->order_by('Elemento', "asc");
		return $this->db->get()->result_array();
	}

	public function ObtenerEscuelas(){
		$this->db->select("cats.Id, cats.Elemento, cate.Elemento AS Subtext");
		$this->db->from('CatSecundarias as cats');
		$this->db->join('CatElementos AS cate', 'cate.Id = cats.IdCatMunicipio', 'inner');
		//$this->db->where('cats.Id !=', '1');
		$this->db->where('cate.Catalogo', 'CatMunicipio');
		$this->db->order_by('cate.Elemento, cats.Elemento', "asc");
		return $this->db->get()->result_array();
	}

	public function Registrar($data){
		$SQL = "INSERT INTO Participantes ("
				.implode(',', array_keys($data)).",FR
				) VALUES ("
				.implode(',', $data ).",'"
				.date('Y-m-d H:i:s')."');";
				
		////echo $SQL;
		////return implode(',', $campusero );
		////return $SQL;
		//return ($this->db->query($SQL)) ? true : false;
		////return ($this->db->query($SQL)) ? $SQL : false;
		
		return ($this->db->query($SQL)) ? true : false;
	}

	public function consultarSiYaSeRegistro($data){
		$nombre		 = str_replace("'","", trim( $data['Nombre']) 	);
		$ap_pa		 = str_replace("'","", trim( $data['Ap_pa']) 		);
		$ap_ma		 = str_replace("'","", trim( $data['Ap_ma']) 		);
		$fecha_nac = str_replace("'","", trim( $data['Fecha_nac']));

		$this->db->select("Id");
		$this->db->from('Participantes');
		$this->db->where('Nombre',		$nombre);
		$this->db->where('Ap_pa',			$ap_pa);
		$this->db->where('Ap_ma',			$ap_ma);
		$this->db->where('Fecha_nac',	$fecha_nac);

		return (count( $this->db->get()->result_array() ) > 0) ? true : false;
	}

}
/* End of file modelName.php */
/* Location: ./application/models/modelName.php */