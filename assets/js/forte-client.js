var forte_buscar = function(data_item){
	tabla.hide();
	msg.hide();
	preload.show();

	data_item.push({name:"action",value:"forte"});
	$.ajax({
		url: forte.ajax_url,
		data:data_item,
		dataType:'json',
		success:function(data){
			if(data.error){
				$("#wpl-forte-items-resultados tbody").empty();
				msg.empty().append(data.msg).show();
				preload.hide();
			}else{
				$("#wpl-forte-items-resultados tbody").empty();
				msg.hide();
				preload.hide();
				muestralista(data);
			}
		}
	});
};

var muestralista = function(data){
	var markup = "{{#datos}}<tr id='data_{{id}}' class='wpl-forte-item-resumen'>"+
			      "<td>{{filiacion}}</td>"+
			      "<td>{{nombre}}</td>"+
			      "<td>{{ing_sep}}</td>"+
			      "<td>{{ingreso_forte}}</td>"+
			      "<td>{{desde}}</td>"+
			      "<td>{{hasta}}</td>"+
			    "</tr>{{/datos}}",
		template = Handlebars.compile(markup),
		html = template(data);

	tabla.children('tbody').append(html);
	tabla.css('display','table');
};


var tabla,preload,msg;
$(document).ready(function() {
	tabla = $("#wpl-forte-items-resultados");
	preload = $("#wpl-forte-preload");
	msg = $("#wpl-forte-msg");


	$("#forte-buscador").submit(function(e){
		e.preventDefault();
		var data = $(this).serializeArray();
		forte_buscar(data);
	});
});