var directorio_buscar = function(pagina){
	directorio_paginacion.empty();
	directorio_resultados.hide();
	directorio_msg.hide();
	directorio_preload.show();

	obj_directorio.page = pagina;
	obj_directorio.action = "directorio";

	$.ajax({
		url: directorio.ajax_url,
		data: obj_directorio,
		type: 'GET',
		dataType:'JSON'
	}).done(function(data){
		if(data.error){
			directorio_resultados.children('tbody').empty();
			directorio_msg.empty().append(data.msg).show();
			directorio_preload.hide();
		}else{
			directorio_resultados.children('tbody').empty();
			directorio_msg.hide();
			directorio_preload.hide();
			directorio_resultados_mostrar(data);
		}
	});
};

var directorio_resultados_mostrar = function(data){
	var markup = "{{#datos}}<tr id='directorio_{{CCT}}' class='wpl-directorio-item'>"+
			      "<td>{{Nombre}}</td>"+
			      "<td>{{CCT}}</td>"+
			      "<td>{{SubNivel}}</td>"+
			      "<td>{{Municipio}}</td>"+
			      "<td>{{Turno}}</td>"+
			      "<td><button value='{{CCT}}'><i class='icon-eye-open'></i></button></td>"+
			    "</tr>{{/datos}}",
		info = "{{#datos}}<div id='{{CCT}}' class='wpl-directorio-item-info'>"+
					"<h3>{{Nombre}}</h3>"+
					"<h4>CCT: {{CCT}}</h4>"+
					"<p><strong>Municipio:</strong> {{Municipio}}</p>"+
					"<p><strong>Localidad:</strong> {{Localidad}}</p>"+
					"<p><strong>Ambito:</strong> {{Ambito}}</p>"+
					"<p><strong>Sostenimiento:</strong> {{Sostenimiento}}</p>"+
					"<p><strong>Nivel:</strong> {{SubNivel}}</p>"+
					"<p><strong>Turno:</strong> {{Turno}}</p>"+
					"<p><strong>Domicilio:</strong> {{domicilio}}</p>"+
					"<p><strong>Teléfono:</strong> {{telefono}}</p>"+
					"<p><strong>Código Postal:</strong> {{codigoPostal}}</p>"+
			   "</div>{{/datos}}",
		template = Handlebars.compile(markup),
		template_info = Handlebars.compile(info),
		html = template(data),
		html_info = template_info(data);

	directorio_paginacion.append(data.paginacion);
	directorio_resultados.children('tbody').append(html);
	directorio_resultados.css('display','table');

	directorio_info.empty();
	directorio_info.append(html_info);
};


var obj_directorio = {
		loc:1,
		niv:0,
		pal1:'',
		pal2:''
	},
	directorio_preload,
	directorio_msg,
	directorio_resultados,
	directorio_paginacion,
	directorio_info;
$(document).ready(function() {
	if($("#wpl-directorio").length>0){
		console.log("Directorio de Instituciones Educativas");

		directorio_preload = $("#wpl-directorio-preload");
		directorio_msg = $("#wpl-directorio-msg");
		directorio_resultados = $("#wpl-directorio-items-resultados");
		directorio_paginacion = $(".wpl-directorio-paginacion");
		directorio_info = $("#wpl-directorio-info");

		$(".wpl-directorio-paginacion").on( "click", "a", function(event){
			event.preventDefault();
			var url = $.url($(this).attr('href'));
			directorio_buscar(url.param('page'));
		});

		$("#wpl-directorio-items-resultados").on('click','button',function(event){
			$.fancybox({
				type:'inline',
				href:'#'+$(this).val(),
				autoSize: false,
				width:500,
				height: 'auto'
			});
		});

		$("#directorio-consultar").click(function(event){
			obj_directorio.loc = $("#loc").val();
			obj_directorio.niv = $("#niv").val();
			obj_directorio.pal1 = $("#pal1").val();
			obj_directorio.pal2 = $("#pal2").val();
			
			directorio_buscar(null);
		});

	}	
});