$(document).ready(function(e) {
	var markup = "{{#datos}}<tr>"+
					"<td>{{no}}</td>"+
					"<td>{{nombre_institucion}}</td>"+
					"<td>{{municipio}}</td>"+
					"<td>{{nivel}}</td>"+
					"<td>{{programas}}</td>"+
					"<td>{{acuerdo}}</td>"+
					"<td>{{periodico_oficial}}</td>"+
					"<td>{{domicilio}}</td>"+
				"</tr>{{/datos}}";

	$("#superior-revoe #btnbuscar").click(function() {
		var criterio=$("#superior-revoe #txtbuscar").val();
		buscardatos(criterio);
	});

	var buscardatos=function(data){
		$("#superior-revoe .preloader").show();
		$.ajax({
			dataType: "json",
			type:"GET",
			data:{action:"busqueda_superior_revoe",busqueda:data},
			url: superior_revoe.ajax_url,
			success: function(datos){
				$("#superior-revoe .preloader").hide();
				if(datos.error==false){
					$("#superior-revoe #listasuperior").css("visibility","visible");
					muestralista(datos)
				}else{
					$("#superior-revoe #listasuperior").css("visibility","hidden");
				}
				$("#superior-revoe #lblerror").html(datos.msg);
			}
		});
	};

	var muestralista=function(data){
		var template = Handlebars.compile(markup),
			html = template(data);
		$("#superior-revoe #listasuperior tbody").empty().append(html);
	}


});