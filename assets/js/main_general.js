$(function(){
    var currentDate; // Holds the day clicked when adding a new event
    var currentEvent; // Holds the event object when editing an event
    $('#color').colorpicker(); // Colopicker
    $('#time').timepicker({
        minuteStep: 5,
        showInputs: false,
        disableFocus: true,
        showMeridian: false
    });  // Timepicker
    var base_url='http://localhost/siged-rvoe/'; // Here i define the base_url
    // Fullcalendar
    $('#calendar').fullCalendar({
        // THIS KEY WON'T WORK IN PRODUCTION!!!
        // To make your own Google API key, follow the directions here:
        // http://fullcalendar.io/docs/google_calendar/
        //  googleCalendarApiKey: 'AIzaSyDcnW6WejpTOCffshGDDb4neIrXVUA1EAE',
        weekends: false,
        //defaultView: 'agendaWeek',
        //hiddenDays: [ 2, 4 ],
        //start: '10:00', // a start time (10am in this example)
        //end: '18:00', // an end time (6pm in this example)
        //dow: [ 1, 2, 3, 4 ],
        firstDay: 1,
             monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
             monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
             dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles',
             'Jueves', 'Viernes', 'Sábado'],
             dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
             buttonText:
              {
                  prev:     ' ◄ ',
                  next:     ' ► ',
                  prevYear: ' &lt;&lt; ',
                  nextYear: ' &gt;&gt; ',
                  today:    'Hoy',
                  month:    'Mes',
                  week:     'Semana',
                  day:      'Día'
              },
        lang: 'es',
        timeFormat: 'H(:mm)',
        header: {
            left: 'prev, next, today',
            center: 'title',
             right: 'month, basicWeek, basicDay'
        },
        //selectable: true,
        //selectHelper: true,
        select: function(start, end) {                
                $('#ModalAdd #start').val(moment(start).format('YYYY-MM-DD HH:mm:ss'));
                $('#ModalAdd #end').val(moment(end).format('YYYY-MM-DD HH:mm:ss'));
                $('#ModalAdd').modal('show');
            },
        // Get all events stored in database
        eventLimit: true, // allow "more" link when too many events
        events: base_url+'Agenda/Calendar/getEvents',
        // Handle Day Click
        // dayClick: function(date, event, view) {
        //     currentDate = date.format();
        //     // Open modal to add event
        //     modal({
        //         // Available buttons when adding
        //         buttons: {
        //             add: {
        //                 id: 'add-event', // Buttons id
        //                 css: 'btn-success', // Buttons class
        //                 label: 'Agregar' // Buttons label
        //             }
        //         },
        //         title: 'Agendar Cita ' + '' + '' // Modal title
        //     });
        // },   
        // editable: true, // Make the event draggable true 
        // eventDrop: function(event, delta, revertFunc) {              
        //     $.post(base_url+'Agenda/Calendar/dragUpdateEvent',{                            
        //         id:event.id,
        //         date: event.start.format()
        //     }, function(result){
        //         if(result)
        //         {
        //         sweetAlert({title:'<div style="position: absolute;left: 0;right: 0; height: 41px;background-color: #5cb85c; margin: 0; font-size: 15px; text-align: center;padding-left: 12px; border-bottom: 1px solid #e5e5e5;"><center><h4 style="font-weight: bold; font-size: 17px;">Mensaje</h4></center></div>',text:'<p style="font-size: 16px;">¡Se modifico cita!</p>',html: true,timer:2000,showConfirmButton:false,type:'success'});
        //         //alert('Se modifico cita');
        //         }
        //         else
        //         {
        //             alert('Try Again later!')
        //         }
        //     });
        //   },
        // Event Mouseover
        eventMouseover: function(calEvent, jsEvent, view){
            var tooltip = '<div class="event-tooltip">' + calEvent.description + '</div>';
            $("body").append(tooltip);
            $(this).mouseover(function(e) {
                $(this).css('z-index', 10000);
                $('.event-tooltip').fadeIn('500');
                $('.event-tooltip').fadeTo('10', 1.9);
            }).mousemove(function(e) {
                $('.event-tooltip').css('top', e.pageY + 10);
                $('.event-tooltip').css('left', e.pageX + 20);
            });
        },
        eventMouseout: function(calEvent, jsEvent) {
            $(this).css('z-index', 8);
            $('.event-tooltip').remove();
        },
        // Handle Existing Event Click
        eventClick: function(calEvent, jsEvent, view) {
            // Set currentEvent variable according to the event clicked in the calendar
            currentEvent = calEvent;
            // Open modal to edit or delete event
            modal({
                // Available buttons when editing
                buttons: {
                    delete: {
                        id: 'delete-event',
                        css: 'btn-danger',
                        label: 'Eliminar'
                    },
                    update: {
                        id: 'update-event',
                        css: 'btn-success',
                        label: 'Modificar'
                    }
                },
                title: 'Editar cita "' + calEvent.title + '"',
                event: calEvent
            });
        }
    });
    // Prepares the modal window according to data passed
    function modal(data) {
        // Set modal title
        $('.modal-title').html(data.title);
        // Clear buttons except Cancel
        $('.modal-footer button:not(".btn-default")').remove();
        // Set input values
        //$('#title').val(data.event ? data.event.title : '');
        if( ! data.event) {
            // When adding set timepicker to current time
            var now = new Date();
            var time = now.getHours() + ':' + (now.getMinutes() < 10 ? '0' + now.getMinutes() : now.getMinutes());
        } else {
            // When editing set timepicker to event's time
            var time = data.event.date.split(' ')[1].slice(0, -3);
            time = time.charAt(0) === '0' ? time.slice(1) : time;
        }
        $('#time').val(time);
        $('#description').val(data.event ? data.event.description : '');
        $('#color').val(data.event ? data.event.color : '#3a87ad');
        // Create Butttons
        $.each(data.buttons, function(index, button){
            $('.modal-footer').prepend('<button type="button" id="' + button.id  + '" class="btn ' + button.css + '">' + button.label + '</button>')
        })
        //Show Modal
        $('.modal').modal('show');
    }
    // Handle Click on Add Button
    $('.modal').on('click', '#add-event',  function(e){
        if(validator(['description'])) {
            $.post(base_url+'Agenda/Calendar/addEvent', {
                title: $('#title').val(),
                description: $('#description').val(),
                id_tramite: $('#id_tramite').val(),
                id_plantel: $('#id_plantel').val(),
                id_nivel: $('#id_nivel').val(),
                color: $('#color').val(),
                date: currentDate + ' ' + getTime()
            }, function(result){
                $('.modal').modal('hide');
                $('#calendar').fullCalendar("refetchEvents");
            });
        }
    });
    // Handle click on Update Button
    $('.modal').on('click', '#update-event',  function(e){
        if(validator(['description'])) {
            $.post(base_url+'Agenda/Calendar/updateEvent', {
                id: currentEvent._id,
                title: $('#title').val(),
                description: $('#description').val(),
                id_tramite: $('#id_tramite').val(),
                id_plantel: $('#id_plantel').val(),
                id_nivel: $('#id_nivel').val(),
                color: $('#color').val(),
                date: currentEvent.date.split(' ')[0]  + ' ' +  getTime()
            }, function(result){
                $('.modal').modal('hide');
                $('#calendar').fullCalendar("refetchEvents");
            });
        }
    });
    // Handle Click on Delete Button
    $('.modal').on('click', '#delete-event',  function(e){
        $.get(base_url+'Agenda/Calendar/deleteEvent?id=' + currentEvent._id, function(result){
            $('.modal').modal('hide');
            $('#calendar').fullCalendar("refetchEvents");
        });
    });
    // Get Formated Time From Timepicker
    function getTime() {
        var time = $('#time').val();
        return (time.indexOf(':') == 1 ? '0' + time : time) + ':00';
    }
    // Dead Basic Validation For Inputs
    function validator(elements) {
        var errors = 0;
        $.each(elements, function(index, element){
            if($.trim($('#' + element).val()) == '') errors++;
        });
        if(errors) {
            $('.error').html('*Campos requeridos:Nombre y Observaciones');
            return false;
        }
        return true;
    }
});
