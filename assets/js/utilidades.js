$(document).ready(function() {

  $(this).on('click', '.loadSubmenu', function(e) {
    e.preventDefault();
    url = $(this).attr('href');
    $('.contenedor-principal').load(url);
       })

    $(this).on('click', '.subcontenido-titulo', function(e) {
        var flecha = $(this).attr('rotate');
        $(this).next().slideToggle();
        $('.'+flecha).toggleClass('rotated');
    })



     $(this).on('click', '.verDatos', function(e) {

    e.preventDefault();
    $.ajax({
      'url': $(this).attr('href'),
      success: function(html){
        alertify.minimalDialog || alertify.dialog('minimalDialog', function(){
          return{
            main:function(content) {
              this.setContent(content);
            }
          };
            //alertify.message('OK');
          });
          alertify.minimalDialog(html);
      }
    })
  })

    $(this).on('click', '.eliminaDoc', function(e) {

      var msj = $(this).attr('mensaje');
      var msjConfirm = $(this).attr('confirmacion');
      var url = $(this).attr('href');
      var btnUp = $(this).attr('btnUp');
      var btnDel = $(this).attr('id');
      var contenidosubidos = $(this).attr('contenidosubidos');
      var contenidoAlerta = $(this).attr('contenidoAlerta');

      e.preventDefault();

      alertify.confirm('<div class="text-center">'+msj+'</div>', function() {
          //swal({title:'<div style="position: absolute;left: 0;right: 0; height: 41px;background-color: #94c120; margin: 0; font-size: 15px; text-align: center;padding-left: 12px; border-bottom: 1px solid #e5e5e5;"><center><h4 style="font-weight: bold; font-size: 17px;">Mensaje</h4></center></div>',text:'<p style="font-size: 16px;">'+msjConfirm+'</p>',html:true,timer:3000,showConfirmButton:false,type:'success'});
          $.post(url);
          var valorActual = $('#'+contenidosubidos).html();
          $('#'+contenidosubidos).html(parseInt(valorActual)-1);
          $('#'+btnUp).removeAttr('disabled');
          $('#'+btnDel).attr('disabled', 'disabled');

          $('#'+contenidoAlerta).children('div').removeClass('alert-success');
          $('#'+contenidoAlerta).children('div').addClass('alert-warning');



        })

    })

    $(this).on('click', '.verData', function(e) {
    var url = $(this).attr('href');
    var pre = document.createElement('div');
    var btnUp = $(this).attr('id');
    var btnDel = $(this).attr('btnDelete');
    var contenidosubidos = $(this).attr('contenidosubidos');
    var totalArchivos = $(this).attr('totalArchivos');
    var contenidoAlerta = $(this).attr('contenidoAlerta');
    e.preventDefault();

    $(pre).load(url, {btnUp:btnUp, btnDel:btnDel, contenidosubidos:contenidosubidos, totalArchivos:totalArchivos, contenidoAlerta:contenidoAlerta});
    pre.style.maxHeight = "auto";
    pre.style.overflowWrap = "break-word";
    pre.style.margin = "-16px -16px -16px 0";
    pre.style.paddingBottom = "24px";
    pre.style.transition = 'zoom';

    alertify.alert(pre, function() {

    }).set({transition:'zoom', title:'Subir Archivo'}).show();
      //$('#'+btnUp).attr('disabled', 'disabled');
      //$(btnDel).removeAttr('disabled');
  })



    $('.formValidate2').validate({
      submitHandler: function(form) {
        var url = $('.formValidate2').attr('action');
        $.ajax({
            type: 'POST',
            url: url,
            data: $('.formValidate2').serialize(),
            success: function(data) {
              $('#result1').html(data);
                //sweetAlert({title:'RESULTADO DE LA OPERACION',text:'¡' + data + '',timer:3000,showConfirmButton:false,type:'success'});
            }
        })
      }
    })

    $('.formValidate3').validate({
      submitHandler: function(form) {
        var url = $('.formValidate3').attr('action');

        $.ajax({
            type: 'POST',
            url: url,
            data: $('.formValidate3').serialize(),
            success: function(data) {
              $('#result2').html(data);
                //sweetAlert({title:'RESULTADO DE LA OPERACION',text:'¡' + data + '',timer:3000,showConfirmButton:false,type:'success'});
            }
        })
      }
    })

  $(this).on('click', '.carga-emergente', function(e) {
    var url = $(this).attr('href');
        var liga = $(this).attr('href');
        var elemento = $(this).attr('id');
        var titulo = $(this).attr('mensaje');

    e.preventDefault();
    $.ajax({
          'url': $(this).attr('href'),
          success: function(html){
            alertify.minimalDialog || alertify.dialog('minimalDialog',function(){

            return {
                main:function(content){
                    this.setContent(content);
                    $('.formulario').submit(function() {
                      var enlace = $(this).attr('href');
                      //var padre  = $(this).atr('padre');
                        $.ajax({
                            type: 'POST',
                            url: $(this).attr('action'),
                            data: $(this).serialize(),
                            success: function(data) {
                              $('#result').html(data);
                              //var table = $('#'+tab).DataTable();
                              //table.ajax.reload();
                              //$('#contenido1').load('<?=base_url();?>Administracion/C_Submenus/verRegistros/<?=$id;?>');
                                        // alertify.set('notifier','position', 'top-right');
                                        // alertify.success('Registro Guardado Correctamente!',('notifier','position'));
                                        sweetAlert({title:'<center><h4 class="text-center">Mensaje</h4></center>',text:'' + data + '',html:true,timer:3000,showConfirmButton:false,type:'info'});
                                        //sweetAlert({title:'Mensaje',text:'' + data + '',timer:3000,showConfirmButton:false,type:'success'});
                                        //cargaTabla();
                                        //load(1);
                                        location.reload(true);
                            }
                        })
                        return false;
                      });
                }
            };
        })
        alertify.minimalDialog(html).set({transition:'zoom',message: 'Transition effect: zoom',title: titulo}).show();
          }
        })
  })

  $('.formulario').submit(function(e) {

        var url = $(this).attr('action');
        e.preventDefault()
        $.ajax({
            type: 'POST',
            url: url,
            data: $(this).serialize(),
            success: function(data) {
              //$('#result').html(data);
                // alertify.set('notifier','position', 'top-right');
                // alertify.success(data,('notifier','position'));
                //sweetAlert({title:'<center><h4 class="text-center">Mensaje</h4></center>',text:'' + data + '',html:true,timer:3000,showConfirmButton:false,type:'info'});
                swal({title:'<div style="position: absolute;left: 0;right: 0; height: 41px;background-color: #94c120; margin: 0; font-size: 15px; text-align: center;padding-left: 12px; border-bottom: 1px solid #e5e5e5;"><center><h4 style="font-weight: bold; font-size: 17px;">Mensaje</h4></center></div>',text:'<p style="font-size: 16px;">¡Registro eliminado!</p>',html:true,timer:3000,showConfirmButton:false,type:'primary'});
                //sweetAlert({title:'Mensaje',text:'' + data + '',timer:3000,showConfirmButton:false,type:'success'});
                location.reload(true);
            }
        }).setHeader('<div style="text-align:center;background-color:#d9534f; margin: -24px; margin-bottom: 0;padding: 1px 24px;"> <h4 style="font-weight: bold; font-size: 20px;">Mensaje</h4> </div> ').set({transition:'zoom'}).show();
        return false;
    });

  $('.formulario-editar').submit(function(e) {
    //var url = $(this).attr('action');
    var reload = $(this).attr('href');
    e.preventDefault();

    $.ajax({
          type: 'POST',
          url: $(this).attr('action'),
          data: $(this).serialize(),
      success: function(data) {
              $('#result').html(data);
              //var table = $('#'+tab).DataTable();
              //table.ajax.reload();
              //$('.contenedor-principal').load(reload);
              //var notification = alertify.notify('Registro Modificado Correctamente!', 'success', 5, function(){  console.log('dismissed'); });
              alertify.set('notifier','position', 'top-right');
         alertify.success('¡Registro modificado!',('notifier','position'));
                location.reload(true);
          }
      })
      return false;
  })

  $(this).on('click', '.eliminar', function(e) {
    var url = $(this).attr('href');
    var estilo=$(this).attr('href');

        e.preventDefault();
    alertify.confirm('<div style="text-align:center;">¿Está seguro de eliminar el registro?</div>', function() {
      //alertify.success('Ok');
            swal({title:'<div style="position: absolute;left: 0;right: 0; height: 41px;background-color: #94c120; margin: 0; font-size: 15px; text-align: center;padding-left: 12px; border-bottom: 1px solid #e5e5e5;"><center><h4 style="font-weight: bold; font-size: 17px;">Mensaje</h4></center></div>',text:'<p style="font-size: 16px;">¡Registro eliminado!</p>',html:true,timer:3000,showConfirmButton:false,type:'success'});
      $.post(url);
      $('.contenedor-principal').load();
            //load(1);
            location.reload(true);
    },
    function() {
      //alertify.error('No se Elimino el Registro');
            swal({title:'<div style="position: absolute;left: 0;right: 0; height: 41px;background-color: #94c120; margin: 0; font-size: 15px; text-align: center;padding-left: 12px; border-bottom: 1px solid #e5e5e5;"><center><h4 style="font-weight: bold; font-size: 17px;">Mensaje</h4></center></div>',text:'<p style="font-size: 16px;">¡Operación cancelada!</p>',html: true,timer:3000,showConfirmButton:false,type:'success'});

    }).setHeader('<div style="text-align:center;background-color:#d9534f; margin: -24px; margin-bottom: 0;padding: 1px 24px;"> <h4 style="font-weight: bold; font-size: 20px;">Mensaje</h4> </div> ').set({transition:'zoom'}).show();

  })

    $(this).on('click', '.cerrar-sesion', function(e) {
       var url = $(this).attr('href');
        var estilo=$(this).attr('href');
        var msj = $(this).attr('mensaje');
        var msjConfirm = $(this).attr('confirmacion');

        e.preventDefault();
        alertify.confirm('<div class="text-center">'+msj+'</div>', function() {
      //alertify.success('Ok');
            swal({title:'<div style="position: absolute;left: 0;right: 0; height: 41px;background-color: #94c120; margin: 0; font-size: 15px; text-align: center;padding-left: 12px; border-bottom: 1px solid #e5e5e5;"><center><h4 style="font-weight: bold; font-size: 17px;">Mensaje</h4></center></div>',text:'<p style="font-size: 16px;">Cerrando sesión</p>',html:true,timer:4500,showConfirmButton:false,type:'success'});
      $.post(url);
      $('.contenedor-principal').load();
            //load(1);
            location.reload(true);
    },
    function() {
      //alertify.error('No se Elimino el Registro');
            swal({title:'<div style="position: absolute;left: 0;right: 0; height: 41px;background-color: #94c120; margin: 0; font-size: 15px; text-align: center;padding-left: 12px; border-bottom: 1px solid #e5e5e5;"><center><h4 style="font-weight: bold; font-size: 17px;">Mensaje</h4></center></div>',text:'<p style="font-size: 16px;">¡Operación cancelada!</p>',html: true,timer:3000,showConfirmButton:false,type:'success'});

    }).setHeader('<div style="text-align:center;background-color:#d9534f; margin: -24px; margin-bottom: 0;padding: 1px 24px;"> <h4 style="font-weight: bold; font-size: 20px;">Mensaje</h4> </div> ').set({transition:'zoom'}).show();

  })

      $(this).on('click', '.confirmar2', function(e) {
       var url = $(this).attr('href');
        var estilo=$(this).attr('href');
        var msj = $(this).attr('mensaje');
        var msjConfirm = $(this).attr('confirmacion');

        e.preventDefault();
        alertify.confirm('<div class="text-center">'+msj+'</div>', function() {
          //alertify.success('Ok');
          swal({title:'<div style="position: absolute;left: 0;right: 0; height: 41px;background-color: #94c120; margin: 0; font-size: 15px; text-align: center;padding-left: 12px; border-bottom: 1px solid #e5e5e5;"><center><h4 style="font-weight: bold; font-size: 17px;">Mensaje</h4></center></div>',text:'<p style="font-size: 16px;">'+msjConfirm+'</p>',html:true,timer:3000,showConfirmButton:false,type:'success'});
          $.post(url);
          $('.contenedor-principal').load();
          //load(1);
          setTimeout('document.location.reload()',2000);
        },
        function() {
          //alertify.error('No se Elimino el Registro');
          swal({title:'<div style="position: absolute;left: 0;right: 0; height: 41px;background-color: #94c120; margin: 0; font-size: 15px; text-align: center;padding-left: 12px; border-bottom: 1px solid #e5e5e5;"><center><h4 style="font-weight: bold; font-size: 17px;">Mensaje</h4></center></div>',text:'<p style="font-size: 16px;">¡Operación cancelada!</p>',html: true,timer:3000,showConfirmButton:false,type:'success'});

        }).setHeader('<div style="text-align:center;background-color:#d9534f; margin: -24px; margin-bottom: 0;padding: 1px 24px;"> <h4 style="font-weight: bold; font-size: 20px;">Mensaje</h4> </div> ').set({transition:'zoom'}).show();

  })

  $(this).on('click', '.actionConfirm', function(e) {
    var url = $(this).attr('href');
    var desactivar = $(this).data('desactivar');
    var elementos = $(this).data('element');
    var mensaje = $(this).data('mensaje');
    var confirmacion = $(this).data('confirmacion');

    e.preventDefault();
    alertify.confirm('<div class="text-center">'+mensaje+'</div>', function() {
      $.ajax({
        url:url,
        type:"POST",
        data: $(this).serialize(),
        success: function(data) {
          respuesta = $.parseJSON(data);
          $.each(respuesta, function(id, valo) {
            swal({
              title:'<div style="position: absolute;left: 0;right: 0; height: 41px;background-color: #94c120; margin: 0; font-size: 15px; text-align: center;padding-left: 12px; border-bottom: 1px solid #e5e5e5;"><center><h4 style="font-weight: bold; font-size: 17px;">Mensaje</h4></center></div>',
              text:'<p style="font-size: 16px;">'+respuesta.mensaje+'</p>',
              html:true,
              timer:3000,
              showConfirmButton:false,
              type:'success'
            });

            if (respuesta.estatus == "true") {

              $.each(elementos, function(indice, valor) {

                $(valor.elemento).attr('disabled', 'disabled');

              });
            }

          })

        }
      })

      //$.post(url);

    },
    function() {
      swal({title:'<div style="position: absolute;left: 0;right: 0; height: 41px;background-color: #94c120; margin: 0; font-size: 15px; text-align: center;padding-left: 12px; border-bottom: 1px solid #e5e5e5;"><center><h4 style="font-weight: bold; font-size: 17px;">Mensaje</h4></center></div>',
        text:'<p style="font-size: 16px;">¡Operación cancelada!</p>',
        html: true,
        timer:3000,
        showConfirmButton:false,
        type:'success'
      });
    }).setHeader('<div style="text-align:center;background-color:#d9534f; margin: -24px; margin-bottom: 0;padding: 1px 24px;"> <h4 style="font-weight: bold; font-size: 20px;">Mensaje</h4> </div> ').set({transition:'zoom'}).show();
  })


    $(this).on('click', '.confirmar', function(e) {
        var url = $(this).attr('href');
        var estilo=$(this).attr('href');
        var msj = $(this).attr('mensaje');
        var msjConfirm = $(this).attr('confirmacion');

        e.preventDefault();
        alertify.confirm('<div class="text-center">'+msj+'</div>', function() {
            //alertify.success('Ok');
            var men = $.post(url);

            $.ajax({
                type: 'POST',
                url: url,
                data: $(this).serialize(),
                success: function(data) {
                    //swal({title:'<div style="position: absolute;left: 0;right: 0; height: 41px;background-color: #94c120; margin: 0; font-size: 15px; text-align: center;padding-left: 12px; border-bottom: 1px solid #e5e5e5;"><center><h4 style="font-weight: bold; font-size: 17px;">Mensaje</h4></center></div>',text:'<p style="font-size: 16px;">'+data+'</p>',html:true,timer:5000,showConfirmButton:false,type:'info'});
                    swal({title:'<div style="position: absolute;left: 0;right: 0; height: 41px;background-color: #94c120; margin: 0; font-size: 15px; text-align: center;padding-left: 12px; border-bottom: 1px solid #e5e5e5;"><center><h4 style="font-weight: bold; font-size: 17px;">Mensaje</h4></center></div>', text:'<center><p style="font-size:16px;">'+msjConfirm+'</p></center>', type:"success", html:true, time: 3000,showCancelButton: false,showConfirmButton:false});
                    setTimeout('document.location.reload()',2000);
                }
            })
            return false;

            //swal({title:'<div style="position: absolute;left: 0;right: 0; height: 41px;background-color: #94c120; margin: 0; font-size: 15px; text-align: center;padding-left: 12px; border-bottom: 1px solid #e5e5e5;"><center><h4 style="font-weight: bold; font-size: 17px;">Mensaje</h4></center></div>',text:'<p style="font-size: 16px;">'+men+'</p>',html:true,timer:4000,showConfirmButton:false,type:'info'});

            //$('.contenedor-principal').load();
            //load(1);
            //location.reload(true);
        },
        function() {
            //alertify.error('No se Elimino el Registro');
            //swal({title:'<div style="position: absolute;left: 0;right: 0; height: 41px;background-color: #94c120; margin: 0; font-size: 15px; text-align: center;padding-left: 12px; border-bottom: 1px solid #94c120;"><center><h4 style="font-weight: bold; font-size: 17px;">Mensaje</h4></center></div>',text:'<p style="font-size: 16px;">¡Operación cancelada!</p>',html: true,timer:4000,showConfirmButton:false,type:'info'});
            //swal({title:"Mensaje", text:"Hasta luego", type:"success", html:true, showCancelButton: false,showConfirmButton:false});

        }).setHeader('<div style="text-align:center;background-color:#d9534f; margin: -24px; margin-bottom: 0;padding: 1px 24px;"> <h4 style="font-weight: bold; font-size: 20px;">Mensaje</h4> </div> ').set({transition:'zoom'}).show();

    })



      $(this).on('click', '.confirmar-return', function(e) {
        var url = $(this).attr('href');
        var estilo=$(this).attr('href');
        var msj = $(this).attr('mensaje');
        var parametro = $(this).attr('parametro');
        //var msjConfirm = $(this).attr('confirmacion');

        e.preventDefault();
        alertify.confirm('<div class="text-center">'+msj+'</div>', function() {
            //alertify.success('Ok');
            var men = $.post(url);

            $.ajax({
                type: 'POST',
                url: url+'&parametro='+parametro,
                data: $(this).serialize(),
                success: function(data) {
                    //swal({title:'<div style="position: absolute;left: 0;right: 0; height: 41px;background-color: #94c120; margin: 0; font-size: 15px; text-align: center;padding-left: 12px; border-bottom: 1px solid #e5e5e5;"><center><h4 style="font-weight: bold; font-size: 17px;">Mensaje</h4></center></div>',text:'<p style="font-size: 16px;">'+data+'</p>',html:true,timer:5000,showConfirmButton:false,type:'info'});
                    swal({title:'<div style="position: absolute;left: 0;right: 0; height: 41px;background-color: #94c120; margin: 0; font-size: 15px; text-align: center;padding-left: 12px; border-bottom: 1px solid #e5e5e5;"><center><h4 style="font-weight: bold; font-size: 17px;">Mensaje</h4></center></div>', text:'<center><p style="font-size:16px;">'+data+'</p></center>', type:"success", html:true, time: 3000,showCancelButton: false,showConfirmButton:false});
                    setTimeout('document.location.reload()',2000);
                }
            })
            return false;

            //swal({title:'<div style="position: absolute;left: 0;right: 0; height: 41px;background-color: #94c120; margin: 0; font-size: 15px; text-align: center;padding-left: 12px; border-bottom: 1px solid #e5e5e5;"><center><h4 style="font-weight: bold; font-size: 17px;">Mensaje</h4></center></div>',text:'<p style="font-size: 16px;">'+men+'</p>',html:true,timer:4000,showConfirmButton:false,type:'info'});

            //$('.contenedor-principal').load();
            //load(1);
            //location.reload(true);
        },
        function() {
            //alertify.error('No se Elimino el Registro');
            swal({title:'<div style="position: absolute;left: 0;right: 0; height: 41px;background-color: #94c120; margin: 0; font-size: 15px; text-align: center;padding-left: 12px; border-bottom: 1px solid #94c120;"><center><h4 style="font-weight: bold; font-size: 17px;">Mensaje</h4></center></div>',text:'<p style="font-size: 16px;">¡Operación cancelada!</p>',html: true,timer:4000,showConfirmButton:false,type:'info'});
            //swal({title:"Mensaje", text:"Hasta luego", type:"success", html:true, showCancelButton: false,showConfirmButton:false});

        }).setHeader('<div style="text-align:center;background-color:#d9534f; margin: -24px; margin-bottom: 0;padding: 1px 24px;"> <h4 style="font-weight: bold; font-size: 20px;">Mensaje</h4> </div> ').set({transition:'zoom'}).show();

    })

    $(this).on('click', '.crea-input', function(e) {

        e.preventDefault();
        //var elemento = $(this);
        var contenedor = $(this).attr('contenedor');
        var input_name = $(this).attr('input-name');
        prueba = $(this).data('prueba');

        console.log(prueba);

        $.each(prueba, function(i, item) {
            console.log(item.PageName);
        });

        //$('.recive-input').append('<div class="form-group"><label for="" class="col-lg-1 control-label">Especialidad</label><div class="col-lg-10"><input type="text" class="form-control" name="' + input_name + '[]" id="" ></div><div class="col-lg-1"><a class="btn btn-danger fa fa-trash"></a></div></div>');

        // <div class="form-group">
        //       <label for="" class="col-lg-1 control-label">Especialidad</label>
        //       <div class="col-lg-10">
        //         <input type="text" class="form-control" name="nombre_plantel" id="" >
        //       </div>
        //     </div>
    })

    $(this).on('click', '.load-post', function(e) {
        e.preventDefault();

        var url = $(this).attr('href');

        //console.log(url);
        $.post(url);

    })

    $(this).on('click', '.fileinput-button', function(e) {

        $('.mensaje').html('');
        $("#progress .progress-bar").css({ "width": "0%" });

    })

    $(this).on('submit', '.formularioFile',function(e) {

        e.preventDefault();

        var inputFileImage = document.getElementById("archivo_id");
        var file = inputFileImage.files[0];
        var data = new FormData();
        var barra = $('.progress-bar');
        var url = $(this).attr('action');
        var elemento = $(this).attr('contenedor');
        var btnUp = $(this).attr('btnUp');
        var btnDel = $(this).attr('btnDel');
        var contenidosubidos = $(this).attr('contenidosubidos');
        var totalArchivos = $(this).attr('totalArchivos');
        var contenidoAlerta = $(this).attr('contenidoAlerta');


        data.append('archivo',file);
        var url = url;

        $.ajax({
            url:url,
            type:'POST',
            contentType:false,
            data:data,
            processData:false,
            cache:false,
            async: true,
            xhr: function () {

                var xhr = new window.XMLHttpRequest();
                //Progrerso de subida
                xhr.upload.addEventListener("progress", function (evt) {

                    if (evt.lengthComputable) {

                        var percentComplete = parseInt((evt.loaded / evt.total) * 100);
                        $('div.progress > div.progress-bar').css({ "width": percentComplete + "%" });

                    }

                }, false);

                //progreso de bajada
                xhr.addEventListener("progress", function (evt) {

                    if (evt.lengthComputable) {

                        var percentComplete = (evt.loaded / evt.total) *100;
                        $("#progress .progress-bar").css({ "width": percentComplete + "%" });

                    }

                },false);
                return xhr;
            },
            success: function(data) {

                $('.mensaje').html('<div class="alert alert-warning">' + data + '</div>');
                //location.href = 'http://localhost/RVOE/Particular/C_Particular/administrarTramite2?usuario_id=1&tramite=1&modalidad=4&tramite_id=9&plantel_id=8&nivel_id=2';
                //setTimeout("location.href = '"+liga+"'",5000);
                $('#'+elemento).attr('disabled', true);
                $('#'+btnUp).attr('disabled', 'disabled');
                $(btnDel).removeAttr('disabled');
                var valorActual = $('#'+contenidosubidos).html();
                $('#'+contenidosubidos).html(parseInt(valorActual)+1);
                //alert(valorActual);

                var total = $('#'+totalArchivos).html();

                if(parseInt(total) == (parseInt(valorActual)+1)) {

                  $('#'+contenidoAlerta).children('div').removeClass('alert-warning');
                  $('#'+contenidoAlerta).children('div').addClass('alert-success');

                }

            },
            error: function(data) {

                $('.mensaje').html('<div class="alert alert-success">Error al Subir el archivo, intentelo de nuevo...</div>');

            }

        });

    });

    $(this).on('click', '.carga-listado2', function(e) {

        url = $(this).attr('href');
        valor = $(this).attr('valor');
        e.preventDefault();
        var pre = document.createElement('div');

        var titulo = '<a>Titulo</a>';
        $(pre).load(url+valor);
        pre.style.maxHeight = "auto";
        pre.style.overflowWrap = "break-word";
        pre.style.margin = "-16px -16px -16px 0";
        pre.style.paddingBottom = "24px";
        pre.style.transition = 'zoom';

        pre.appendChild(document.createTextNode($('#la').text()));


        alertify.confirm(pre, function(){

            var checkboxValues = new Array();
            //recorremos todos los checkbox seleccionados con .each
            $('input[name="documentos[]"]:checked').each(function() {
                //$(this).val() es el valor del checkbox correspondiente
                console.log($(this).val());
                var archivo = $(this).val();
                window.open('assets/archivos/barra_materiales/'+archivo+'.pdf');
                //$.post('C_Welcome/download/'+archivo);
            });

        }).set({transition:'zoom', title:titulo}).show();

    })

    $('[data-toggle="tooltip"]').tooltip();


    $(this).on('click', '.busqueda', function(e) {
        e.preventDefault();
        var campo = $(this).attr('campo');
        var url = $(this).attr('href');

        window.location.href= url+campo;

    })

    $(this).on('click', '.busqueda-nivel2', function(e) {

        e.preventDefault();
        var url = $(this).attr('href');
        var campo = $(this).data('campo');
        var contenedor = $(this).data('contenedor');

        $('.'+contenedor).load(url+campo);

    })

    $('select.lista-padre2').on('change', function(e){

      var contenedor = $(this).attr('contenedor'); //Select donde se cargara la lista generada
      var tabla = $(this).attr('tabla');           //Tabla que consultaremos
      var campoPadre = $(this).attr('campoPadre'); //Campo que se relaciona la tabla hijo con la tabla padre
      var valor = $(this).val();                   //Valor que obtenemos del evento change
      var txtLista = $(this).attr('txtLista');     //Campo que mostraremos en el option
      var valueLista = $(this).attr('valueLista'); //Valor que daremos al value del option

      $(contenedor).load('listaDependiente/', {tabla:tabla,campoPadre:campoPadre,valor:valor,txtLista:txtLista,valueLista:valueLista});



    })

    $('select.lista-padre').on('change', function(e) {

      var indicePadre = $(this).val();
      var tabla = $(this).attr('tabla'); //Tabla de la que generaremos la lista
      var campoFiltro = $(this).attr('campoFiltro'); //Campo por el que filtraremos la lista
      var valor = $(this).attr('valor'); //valor que pondremos en el value del option (campo de la tabla)
      var texto = $(this).attr('texto'); //Texto que pondremos  en el option (campo de la tabla)
      var elementoLista = $(this).attr('elementoLista'); //elemento (identificado por id) en el que cargaremos nuestra lista
      var controlador = $(this).attr('controlador'); //Funcion que ejecutara (url)

      $(elementoLista).load(controlador, {estado_id:indicePadre,tabla:tabla,campo:campoFiltro,optionValue:valor,optionText:texto});

    })

    $('select.lista-dependiente').on('change', function(e) {

        var valor = $(this).val();
        var contenedor = $(this).attr('contenedor');
        var datos = $(this).attr('datos');
        var campo = $(this).attr('campo');
        var campo_lista = $(this).attr('campo-lista');


        $(contenedor).load(controlador,{estado_id:valor,tabla:datos,campo:campo,campo_lista:campo_lista});

    })

    // $('a').mouseenter(function() {           #funcion para responciveVoice

    //     responsiveVoice.cancel();
    //     responsiveVoice.speak($(this).text(), "Spanish Female");

    // })

    $(this).on('click', '.agregarFecha', function(e) {

      var textoOption = $(this).text();
      var valueOption = $(this).attr('value');
      var contenedorHijo = $(this).attr('contenedorHijo');
      var btnTramites = $(this).attr('btnTramites');

      $(btnTramites).prop('disabled', false);

      $(contenedorHijo).each(function(indice, elemento) {

        $(elemento).attr("href", elemento.href+'&fechafiscal_id='+valueOption);

      });

    })

    $(this).on('click', '.busqueda-por', function(e) {

      e.preventDefault(); //Prevenimos el evento
      var campo = $(this).attr('campo'); //Obtenemos el atributo campo del elemento actual
      var icono = campo.replace('.', '-');
      var texto = $(this).text(); //Obtenemos el texto impreso en el elemento actual

      $('.input-busqueda').attr('placeholder', 'Buscar por '+texto); //Modificamos el atributo placeholder del elemento con clase .input-busqueda para mostrar cuál sera el campo por el que se realizará la búsqueda
      $('.campo-busqueda').attr('value', campo); //Asignamos el valor del campo por el que realizara la busqueda al elemento input para que sea enviado por POST al controlador.
      $('i').removeClass('fa-search');
      $('#'+icono).next().addClass('fa-search');

    })

    /*Configuración de registros a mostrar en tablas*/
    $('select#verRegistros').on('change',function(e) {

      var registros = $(this).val();
      var metodo = $(this).attr('metodo');

      $.post(metodo, {registros:registros});
      location.reload(true);

    });

  /*Carga de contenido en ventana emergente*/
  $(this).on('click', '.divLoad', function(e) {

    e.preventDefault();
    var pre = document.createElement('div');
    var url = $(this).attr('href');
    var titulo = $(this).data('titulo');

    var titulo = '<h4>' + titulo + '</h4>';
    $(pre).load(url);
    pre.style.maxHeight = "auto";
    pre.style.minHeight = "auto";
    pre.style.overflowWrap = "break-word";
    pre.style.margin = "-5px -16px -16px 0";
    pre.style.paddingBottom = "24px";
    pre.style.transition = 'zoom';

    pre.appendChild(document.createTextNode($('#la').text()));
    alertify.confirm(pre, function(){


    }).set({transition:'zoom', title:titulo}).show();

  })




})

  function load(page, liga) {

    $('#loader').fadeIn('slow');

    $.ajax({
      url:liga,
      data: 'action=ajax&page='+page,
      beforeSend: function(objeto) {
        //alert(objeto);
        //$('#loader').html("<img class='fa fa-search'>");
      },
      success:function(data) {
        //alert(data);
        $(".outer").html(data).fadeIn('slow');
        $('#loader').html(data);
      }
    })
  }

function lcontainer(contenido, url) {

    $.ajax({

        url:url,
        beforeSend: function(objeto) {

        },
        success:function(data) {

            $('.contenido-admin').hide();
            $(contenido).html(data);
            $(contenido).show();

        }

    })
}

function formulario(type, action, dat) {

    $.ajax({

        type: type,
        url: action,
        data: dat,
        success: function(data) {

            $('#result').html(data);
            sweetAlert({title:'<center><h4 class="text-center">Mensaje</h4></center>',text:'¡Registro dado de alta!' + data + '',html:true,timer:3000,showConfirmButton:false,type:'primary'});
            //sweetAlert({title:'Mensaje',text:'¡Registro dado de alta!' + data + '',timer:3000,showConfirmButton:false,type:'success'});

        }

    })
}

function confirRegistros(parametros, url) {

    $.post(url,parametros);
    location.reload(true);

}