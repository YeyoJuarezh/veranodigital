var becas_buscar = function(data_item){
	becas_tabla.hide();
	becas_msg.hide();
	becas_preload.show();

	data_item.push({name:"action",value:"becas"});

	$.ajax({
		url: becas.ajax_url,
		data:data_item,
		dataType:'json',
		success:function(data){
			if(data.error){
				becas_tabla.children('tbody').empty();
				becas_msg.empty().append(data.msg).show();
				becas_preload.hide();
			}else{
				becas_tabla.children('tbody').empty();
				becas_msg.hide();
				becas_preload.hide();
				becas_muestralista(data);
			}
		}
	});
};

var becas_muestralista = function(data){
	var markup = "{{#datos}}<tr id='data_{{id}}' class='wpl-becas-item-resumen'>"+
			      "<td>{{municipio}}</td>"+
			      "<td>{{curp}}</td>"+
			      "<td>{{nombre}}</td>"+
			      //"<td>{{cct}}</td>"+
			      "<td>{{programa}}</td>"+
			      //"<td>{{cheque}}</td>"+
			    "</tr>{{/datos}}",
		template = Handlebars.compile(markup),
		html = template(data);

	becas_tabla.children('tbody').append(html);
	becas_tabla.css('display','table');
};


var becas_tabla,becas_preload,becas_msg;

$(document).ready(function() {
	becas_tabla = $("#wpl-becas-items-resultados");
	becas_preload = $("#wpl-becas-preload");
	becas_msg = $("#wpl-becas-msg");

	$("#becas-buscador").submit(function(e){
		e.preventDefault();
		var data = $(this).serializeArray();
		becas_buscar(data);
	});
});