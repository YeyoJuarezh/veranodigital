$(document).ready(function(){
		Obtener_Imagenes();
}); /* END $(document).ready */

function Abrir_Imagen( img_src ){
		$('#VM_Galeria').find('#Galeria_img_seleccionada').attr('src', img_src);
}

function Obtener_Imagenes(){
		var urlPHP = "C_Galeria/Obtener_Imagenes/";
		var datos = {	};
		
		AjaxFunction(urlPHP, datos, function(response){
				var base_url = $("#Galeria").attr('value') + 'assets/img/verano_digital/Galeria/';
				var response_length = response.length;

				if( response_length > 0 ){
						var imagenes = '';

						for(	var i = 0; i < response_length; i++	){
								imagenes += 	'<img';
								imagenes += 	' class="img-thumbnail"';
								imagenes += 	' onclick="Abrir_Imagen(this.src);"';
								imagenes += 	' data-toggle="modal"';
								imagenes += 	' data-target="#VM_Galeria"';
								imagenes += 	' alt="..."';
								// imagenes += 	' src="http://via.placeholder.com/'+T+'x'+T+'"';
								imagenes += 	' src="'+ base_url+response[i] +'"';
								imagenes += 	'>';
						}

						$("#Galeria_imagenes").html(imagenes);
				}
		});
}
