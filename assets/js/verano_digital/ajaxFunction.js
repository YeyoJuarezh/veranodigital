// Funcion principal de AJAX
function AjaxFunction(urlPHP, datos, functionDone = null, functionFail = null){
	var request;
	if( request ){ request.abort(); }

	request = $.ajax({
		// En data puedes utilizar un objeto JSON, un array o un query string
		data		: datos,
		//Cambiar a type: POST si necesario
		type		: "POST",
		// Formato de datos que se espera en la respuesta
		dataType: "json",
		// URL a la que se enviará la solicitud Ajax
		url			: urlPHP
	});

	request.done(function (response, textStatus, jqXHR){
		(functionDone != null) ? functionDone(response) : console.log("Done! : " + response);
	});

	request.fail(function(jqXHR, textStatus, thrown){
		(functionFail != null) ? functionFail(textStatus) : console.log("Error! : " + textStatus);
	});

	//request.always(function(){ console.log('Termino la ejecucion de ajax');	});
}
