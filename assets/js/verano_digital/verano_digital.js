// CONSTANTES
const $formularioRegistro = $("form[name=form_registro]");
$('#btnOtroPoblado').prop("disabled", true);

//https://www.youtube.com/watch?v=DZ9U4e_Rw9c		
//toastr.success('Success!');
//toastr.warning('Success!');
//toastr.error('Success!');

$(document).ready(function(){
	$("[data-toggle='tooltip']").tooltip({
		placement: $(this).data("placement") || 'left'
	});

	// V_VDNav
	$('.navButtons').mouseover(function () {
		$(this).attr('src', $(this).data("hover"));
	}).mouseout(function () {
		$(this).attr('src', $(this).data("src"));
	});

	// V_Inicio
	$("#btnModal1").on("click", function(e){
		$("#imgModal1").css("display", "block");
		$("#imgModal2").css("display", "none");
		$("#imgModal3").css("display", "none");
		e.preventDefault();
	});
	$("#btnModal2").on("click", function(e){
		$("#imgModal1").css("display", "none");
		$("#imgModal2").css("display", "block");
		$("#imgModal3").css("display", "none");
		e.preventDefault();
	});
	$("#btnModal3").on("click", function(e){
		$("#imgModal1").css("display", "none");
		$("#imgModal2").css("display", "none");
		$("#imgModal3").css("display", "block");
		e.preventDefault();
	});


	// V_Registro
	$("#btnOtroPoblado").on("click", function(e){
		$("#nuevoPoblado").css("display", "block");
		e.preventDefault();
	});

	// Botón dentro de ventana modal de Registro
	$("#guardarOtro").on("click", function(e){
		$("#idCatPoblado").val(1);
		$('.modalOtros').modal('toggle');
		e.preventDefault();
	});

	$("#idCatMunicipio").change(function(){
		var urlPHP = "C_Registro/ObtenerLocalidades/";
		var datos = {	"IdMunicipio"	:	$("#idCatMunicipio").val()	};
		
		AjaxFunction(urlPHP, datos, function(response){
			if( $("#idCatMunicipio").val() == null ){
				response = 	'<option value="0" disabled selected>LOCALIDAD</option>';
			}else{
				response = 	'<option value="0" disabled selected>LOCALIDAD</option>' + 
							'<option value="1">- OTRA -</option>' + response;				
			}

			$("#idCatPoblado").html(response);
			$("#idCatPoblado").find('option:nth-child(3)').prop('selected',true).trigger('change');
		});
	});
	

	$("#idCatPoblado").change(function(){
		if($(this).val() != 1){
			$("#nuevoPoblado").val('');
			$('#btnOtroPoblado').prop("disabled", true);
		}else{
			$('#btnOtroPoblado').prop("disabled", false);
		}
	});


	$("#continuarRegistro").on("click", function(e){
		registrarParticipante( ObtenerJSONElementosDelFormulario() );
	});

	$("#registrarParticipante").on("click", function(e){

		if( ValidarCampos() ){
			modalAlert({
				"modalType"				:	"errorModal",
				"textoTitulo"			:	"¡Error en el formulario!",
				"textoContenido"	:	"HAY CAMPOS QUE NO SE HAN RELLENADO CORRECTAMENTE",
				"footerButtons"		:	false
			});

		}else{
			// Consultar si el participante ya ha sido registrado previamente.
			var urlPHP = "C_Registro/ConsultarParticipanteDuplicado/";
			var datos = ObtenerJSONElementosDelFormulario();

			AjaxFunction(urlPHP, datos, function(response){
				//console.log("Usuario previamente registrado " + response);
				if(response){

					modalAlert({
						"modalType"				:	"warningModal",
						"textoTitulo"			:	"¡Advertencia!",
						"textoContenido"	:	"YA SE ENCUENTRA REGISTRADO UN USUARIO CON ESTOS DATOS",
						"footerButtons"		:	true
					});
					// Si se presiona el botón Registrar // Mandar llamar el insert.
				}else{
					registrarParticipante( datos );
				}
			},function(textStatus){
				modalAlert({
					"modalType"				:	"errorModal",
					"textoTitulo"			:	"¡Error!",
					"textoContenido"	:	"HA OCURRIDO UN ERROR",
					"footerButtons"		:	false
				});
				console.log(textStatus);
			});

		}
		e.preventDefault();
	});

	OnlyCharactersAndNumbers("#nombre");
	OnlyCharactersAndNumbers("#ap_pa");
	OnlyCharactersAndNumbers("#ap_ma");
	OnlyNumbers('#grado');
	OnlyNumbers('#telefono');



/////////////////////////////////
//     BOTONES DE ALERTA - PRUEBA

			$("#btn11").on("click", function(e){
				//$('#successModal').modal('show');
				modalAlert({
					"modalType"			:	"successModal",
					"textoTitulo"		:	"El registro se ha guardado correctamente",
					"textoContenido" 	:	"¡MUCHAS GRACIAS POR TU PARTICIPACIÓN!",
					"footerButtons"		:	false
				});
				e.preventDefault();
			});

			$("#btn22").on("click", function(e){
				//$('#errorModal').modal('show');
				modalAlert({
					"modalType"			:	"errorModal",
					"textoTitulo"		:	"¡Error!",
					"textoContenido" 	:	"NO SE HA PODIDO GUARDAR",
					"footerButtons"		:	false
				});
				e.preventDefault();
			});

			$("#btn33").on("click", function(e){
				//$('#warningModal').modal('show');
				modalAlert({
					"modalType"			:	"warningModal",
					"textoTitulo"		:	"¡Advertencia!",
					"textoContenido"	:	"YA SE ENCUENTRA REGISTRADO UN USUARIO CON ESTOS DATOS",
					"footerButtons"		:	true
				});
				e.preventDefault();
			});


// END BOTONES DE ALERTA - PRUEBA
/////////////////////////////////





	$("#limpiar").on("click", function(e){
		LimpiarFormulario();
			//$('.inicioModal').modal('show');
			//$('.inicioModal').modal('toggle');
			//$('.inicioModal').modal('hide');
		e.preventDefault();
	});

	//$("#nombre").focus();

	DeshabilitarControles();
}); /* END $(document).ready */


function DeshabilitarControles(){

	$formularioRegistro.find(".form_input").each(function() {
		$(this).prop("disabled", true);
	});

	$('#registrarParticipante').prop("disabled", true);
	$('#limpiar').prop("disabled", true);
}


function registrarParticipante( datos ){
	urlPHP = "C_Registro/Registrar/";
	datos = datos;

	AjaxFunction(urlPHP, datos, function(response){
		//console.log("Se Registro nuevo usuario " + response);
		
		if(response){
			modalAlert({
				"modalType"				:	"successModal",
				"textoTitulo"			:	"El registro se ha guardado correctamente",
				"textoContenido" 	:	"¡MUCHAS GRACIAS POR TU PARTICIPACIÓN!",
				"footerButtons"		:	false
			});
			LimpiarFormulario();
		}else{
			modalAlert({
				"modalType"				:	"errorModal",
				"textoTitulo"			:	"¡Error!",
				"textoContenido"	:	"NO SE HA PODIDO GUARDAR",
				"footerButtons"		:	false
			});
		}

	},function(textStatus){
		modalAlert({
			"modalType"				:	"errorModal",
			"textoTitulo"			:	"¡Error!",
			"textoContenido"	:	"HA OCURRIDO UN ERROR",
			"footerButtons"		:	false
		});
		console.log(textStatus);
	});
}

function ObtenerJSONElementosDelFormulario() {
	var myArray = new Object(); // creamos un objeto
	
	$formularioRegistro.find(".form_input").each(function() {
		var columnaID = $(this).attr('id');

		if (columnaID != 'idCatMunicipio' && columnaID != undefined) {
			var indice = columnaID.charAt(0).toUpperCase() + columnaID.slice(1);
			var valor  = $.trim($(this).val());

			if(columnaID == 'nombre' || columnaID == 'ap_pa' || columnaID == 'ap_ma'){
				valor = valor.toUpperCase();
			}else if (columnaID == 'email' || columnaID == 'email_tutor') {
				valor = valor.toLowerCase();
			}

			myArray[ indice ] = "'"+valor+"'";
		}

	});

	if($('#idCatPoblado').val() == 1){
		myArray['NuevoPoblado'] = "'"+$.trim($('#nuevoPoblado').val())+"'";
		myArray['IdCatMunicipio'] = "'"+$("#idCatMunicipio").val()+"'";
	}

	// Transofmra a un objeto raro JSON de 2 dimesiones QUE SI JALA.
	return jQuery.makeArray(myArray)[0];

	// Convierte en un JSON plano igual a un simple string
	//return JSON.stringify(myArray);
}

function ValidarCampos(){
	var error = false;
	var $idElementoError = "";

	// Valida que no exista algun input vacío
	$formularioRegistro.find("input").each(function() {

		// Valida que no esten vacios los inputs.
		if( $(this).val() == "" && 
			$(this).attr('id') != undefined && 
			$(this).attr('id') != 'telefono' &&
			$(this).attr('id') != 'email_tutor'
		){
			$(this).parent().addClass("has-error");

			if($idElementoError == ""){
				$(this).focus();
				$idElementoError = $(this).attr('id');
				console.log('inputs vacios ' + $idElementoError);
			}
			error = true;
			
		}else{
			$(this).parent().removeClass("has-error");
		}

		// Valida que esten bien escritos los emails.
		if($(this).attr('id') == 'email' ){
				if(ValidarEmail('#'+$(this).attr('id'))){
					$(this).parent().addClass("has-error");

					if($idElementoError == ""){
						$(this).focus();
						$idElementoError = $(this).attr('id');
						console.log('email mal escrito ' + $idElementoError);
					}
					error = true;
				}
		}
		else if ($(this).attr('id') == 'email_tutor') {
			if ( $(this).val().length > 0 ){
				if(ValidarEmail('#'+$(this).attr('id'))){
					$(this).parent().addClass("has-error");

					if($idElementoError == ""){
						$(this).focus();
						$idElementoError = $(this).attr('id');
						console.log('email mal escrito ' + $idElementoError);
					}
					error = true;
				}
			}
		}
	});

		if($("#idCatGenero").val() == null) {
			console.log('error en idCatGenero ' + $("#idCatGenero").val())
			error = true;
		}
		if($("#idCatPoblado").val() == null) {
			console.log('error en IdCatPoblado ' + $("#idCatPoblado").val())
			error = true;
		}
		if($("#idCatEscuela").val() == null) {
			console.log('error en IdCatEscuela ' + $("#idCatEscuela").val())
			error = true;
		}
	
	if($("#idCatPoblado").val() == 1){
		if($.trim($("#nuevoPoblado").val()) == ''){
			console.log('error en nuevoPoblado ' + $("#nuevoPoblado").val())
			error = true;
		}
		if($("#idCatMunicipio").val() == null){
			console.log('error en idCatMunicipio/Modal' + $("#idCatMunicipio").val() );
			error = true;
		}
	}

	if($("#telefono").val().length > 0){
		if( $("#telefono").val().length < 10){
			console.log('error en telefono, la cantidad de caracteres es menor a 10' /*+ $("#telefono").val().length*/ );
			error = true;
			$("#telefono").parent().addClass("has-error");
		}		
	}

	return error;
}

function LimpiarFormulario(){
	$formularioRegistro.find(".form_input").each(function() {
		$(this).val("");
	});

	$formularioRegistro.find('.form_input').find('li').removeClass("selected").removeClass("active");
	$formularioRegistro.find('select').find('option:nth-child(1)').prop('selected',true).trigger('change'); //trigger a change instead of click
	$formularioRegistro.find('.form_input').find('li').first().addClass("selected").addClass("active");
	
	$formularioRegistro.find("#idCatPoblado").val(0);
	$formularioRegistro.find("input").parent().removeClass("has-error");

	$("#nombre").focus();
}

function modalAlert( modalInfo ){
	//alert( modalInfo['textoTitulo'] );
	$('#IconoModal').removeClass("fa-check").removeClass("fa-times").removeClass("fa-exclamation-triangle");

		switch ( modalInfo['modalType'] ) {
			case "successModal":
				$("#modalGenerico div:first-child div:first-child").css("background-color", "#A1C108");
				$('#IconoModal').addClass("fa-check");					break;

			case "errorModal":
				$("#modalGenerico div:first-child div:first-child").css("background-color", "#EB899B");
				$('#IconoModal').addClass("fa-times");					break;

			case "warningModal":
				$("#modalGenerico div:first-child div:first-child").css("background-color", "#C3CB68");
				$('#IconoModal').addClass("fa-exclamation-triangle");	break;
		}

		$('#TextoModalTitulo').html( modalInfo['textoTitulo'] );
		$('#TextoModalContenido').html( modalInfo['textoContenido'] );

		if( modalInfo['footerButtons'] ){
			$(".modalSinFooter").hide();
			$(".modalConFooter").show();
		}else{
			$(".modalSinFooter").show();
			$(".modalConFooter").hide();
		}

	$('#modalGenerico').modal('show');
}