$(document).ready(function(e) {
	var markup = "{{#datos}}<tr>"+
					"<td>{{no}}</td>"+
					"<td>{{institucion}}</td>"+
					"<td>{{cobertura}}</td>"+
					"<td>{{modalidad}}</td>"+
					"<td>{{no_acuerdo}}</td>"+
				"</tr>{{/datos}}";

    $("#media-revoe #btnbuscar").click(function() {
       var criterio=$("#media-revoe #txtbuscar").val();
	   buscardatos(criterio);
    });

	var buscardatos=function(data){
		$("#media-revoe .preloader").show();
		$.ajax({
			dataType: "json",
			type:"GET",
			data:{action:"busqueda_media_revoe",busqueda:data},
			url: media_superior_revoe.ajax_url,
			success: function(datos){
				$("#media-revoe .preloader").hide();
				if(datos.error==false){
				  	$("#media-revoe #listamedia").css("visibility","visible");
				  	muestralista(datos)
				}else{
					$("#media-revoe #listamedia").css("visibility","hidden");
				}
				$("#media-revoe #lblerror").append(datos.msg);
			}
		});
	};
		
	var muestralista = function(data){
		var template = Handlebars.compile(markup),
			html = template(data);
		$("#media-revoe #listamedia tbody").empty().append(html);



		//jQuery.template("medialist", markup );
		//jQuery.tmpl("medialist", data.datos ).appendTo( "#media-revoe #listamedia tbody");
	}	
	
});	