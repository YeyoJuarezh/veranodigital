$(document).ready(function() {

	$(this).on('click', '.loadSubmenu', function(e) {
		e.preventDefault();
		url = $(this).attr('href');
		$('.contenedor-principal').load(url);
	})

    $(this).on('click', '.subcontenido-titulo', function(e) {
        var flecha = $(this).attr('rotate');
        $(this).next().slideToggle();
        $('.'+flecha).toggleClass('rotated');
    })



	$(this).on('click', '.verDatos', function(e) {

		e.preventDefault();
		$.ajax({
			'url': $(this).attr('href'),
			success: function(html){
				alertify.minimalDialog || alertify.dialog('minimalDialog', function(){
					return{
						main:function(content) {
							this.setContent(content);
						}
					};
    				//alertify.message('OK');
  				});
  				alertify.minimalDialog(html);
			}
		})
	})

	$(this).on('click', '.carga-emergente', function(e) {
		var url = $(this).attr('href');
		e.preventDefault();
		$.ajax({
        	'url': $(this).attr('href'),
        	success: function(html){
        		alertify.minimalDialog || alertify.dialog('minimalDialog',function(){
    				return {
        				main:function(content){
            				this.setContent(content);
            				$('.formulario').submit(function() {
            					var enlace = $(this).attr('href');
            					//var padre  = $(this).atr('padre');
                				$.ajax({
                  					type: 'POST',
                  					url: $(this).attr('action'),
                  					data: $(this).serialize(),
                  					success: function(data) {
                    					$('#result').html(data);
                    					//var table = $('#'+tab).DataTable();
                    					//table.ajax.reload();
                    					//$('#contenido1').load('<?=base_url();?>Administracion/C_Submenus/verRegistros/<?=$id;?>');
                                        // alertify.set('notifier','position', 'top-right');
                                        // alertify.success('Registro Guardado Correctamente!',('notifier','position'));
                                        sweetAlert({title:'Mensaje',text:'' + data + '',timer:3000,showConfirmButton:false,type:'success'});
                                        //cargaTabla();
                                        //load(1);
                                        location.reload(true);
                  					}
                				})
              					return false;
              				});
        				}
    				};
				})
				alertify.minimalDialog(html).set({transition:'zoom',message: 'Transition effect: zoom'}).show();
        	}
      	})
	})

	$('.formulario').submit(function(e) {

        var url = $(this).attr('action');
        e.preventDefault()
        $.ajax({
            type: 'POST',
            url: url,
            data: $(this).serialize(),
            success: function(data) {
        	    $('#result').html(data);
                // alertify.set('notifier','position', 'top-right');
                // alertify.success(data,('notifier','position'));
                sweetAlert({title:'Mensaje',text:'' + data + '',timer:3000,showConfirmButton:false,type:'success'});
                location.reload(true);
            }
        })
        return false;
    });

	$('.formulario-editar').submit(function(e) {
		//var url = $(this).attr('action');
		var reload = $(this).attr('href');
		e.preventDefault();

		$.ajax({
        	type: 'POST',
        	url: $(this).attr('action'),
        	data: $(this).serialize(),
			success: function(data) {
            	$('#result').html(data);
            	//var table = $('#'+tab).DataTable();
            	//table.ajax.reload();
            	//$('.contenedor-principal').load(reload);
            	//var notification = alertify.notify('Registro Modificado Correctamente!', 'success', 5, function(){  console.log('dismissed'); });
            	alertify.set('notifier','position', 'top-right');
 				alertify.success('¡Registro modificado!',('notifier','position'));
                location.reload(true);
        	}
    	})
    	return false;
	})

	$(this).on('click', '.eliminar', function(e) {
		var url = $(this).attr('href');
		var estilo=$(this).attr('href');

        e.preventDefault();
		alertify.confirm('<div class="text-center">¿Está seguro de eliminar el registro?</div>', function() {
			//alertify.success('Ok');
            swal({title:'<div style="position: absolute;left: 0;right: 0; height: 41px;background-color: #5cb85c; margin: 0; font-size: 15px; text-align: center;padding-left: 12px; border-bottom: 1px solid #e5e5e5;"><center><h4 style="font-weight: bold; font-size: 17px;">Mensaje</h4></center></div>',text:'<p style="font-size: 16px;">¡Registro eliminado!</p>',html:true,timer:3000,showConfirmButton:false,type:'success'});
			$.post(url);
			$('.contenedor-principal').load();
            //load(1);
            location.reload(true);
		},
		function() {
			//alertify.error('No se Elimino el Registro');
            swal({title:'<div style="position: absolute;left: 0;right: 0; height: 41px;background-color: #5cb85c; margin: 0; font-size: 15px; text-align: center;padding-left: 12px; border-bottom: 1px solid #e5e5e5;"><center><h4 style="font-weight: bold; font-size: 17px;">Mensaje</h4></center></div>',text:'<p style="font-size: 16px;">¡Operación cancelada!</p>',html: true,timer:3000,showConfirmButton:false,type:'success'});

		}).setHeader('<div style="text-align:center;background-color:#d9534f; margin: -24px; margin-bottom: 0;padding: 1px 24px;"> <h4 style="font-weight: bold; font-size: 20px;">Mensaje</h4> </div> ').set({transition:'zoom'}).show();

	})

    $(this).on('click', '.confirmar', function(e) {
        var url = $(this).attr('href');
        var estilo=$(this).attr('href');
        var msj = $(this).attr('mensaje');
        var msjConfirm = $(this).attr('confirmacion');

        e.preventDefault();
        alertify.confirm('<div class="text-center">'+msj+'</div>', function() {
            //alertify.success('Ok');
            swal({title:'<div style="position: absolute;left: 0;right: 0; height: 41px;background-color: #5cb85c; margin: 0; font-size: 15px; text-align: center;padding-left: 12px; border-bottom: 1px solid #e5e5e5;"><center><h4 style="font-weight: bold; font-size: 17px;">Mensaje</h4></center></div>',text:'<p style="font-size: 16px;">'+msjConfirm+'</p>',html:true,timer:3000,showConfirmButton:false,type:'success'});
            $.post(url);
            $('.contenedor-principal').load();
            //load(1);
            location.reload(true);
        },
        function() {
            //alertify.error('No se Elimino el Registro');
            swal({title:'<div style="position: absolute;left: 0;right: 0; height: 41px;background-color: #5cb85c; margin: 0; font-size: 15px; text-align: center;padding-left: 12px; border-bottom: 1px solid #e5e5e5;"><center><h4 style="font-weight: bold; font-size: 17px;">Mensaje</h4></center></div>',text:'<p style="font-size: 16px;">¡Operación cancelada!</p>',html: true,timer:3000,showConfirmButton:false,type:'success'});

        }).setHeader('<div style="text-align:center;background-color:#d9534f; margin: -24px; margin-bottom: 0;padding: 1px 24px;"> <h4 style="font-weight: bold; font-size: 20px;">Mensaje</h4> </div> ').set({transition:'zoom'}).show();

    })

    $(this).on('click', '.crea-input', function(e) {

        e.preventDefault();
        //var elemento = $(this);
        var contenedor = $(this).attr('contenedor');
        var input_name = $(this).attr('input-name');
        prueba = $(this).data('prueba');

        console.log(prueba);

        $.each(prueba, function(i, item) {
            console.log(item.PageName);
        });

        //$('.recive-input').append('<div class="form-group"><label for="" class="col-lg-1 control-label">Especialidad</label><div class="col-lg-10"><input type="text" class="form-control" name="' + input_name + '[]" id="" ></div><div class="col-lg-1"><a class="btn btn-danger fa fa-trash"></a></div></div>');

        // <div class="form-group">
        //       <label for="" class="col-lg-1 control-label">Especialidad</label>
        //       <div class="col-lg-10">
        //         <input type="text" class="form-control" name="nombre_plantel" id="" >
        //       </div>
        //     </div>
    })

    $(this).on('click', '.load-post', function(e) {
        e.preventDefault();

        var url = $(this).attr('href');

        console.log(url);
        $.post(url);

    })

    $(this).on('click', '.fileinput-button', function(e) {

        $('.mensaje').html('');
        $("#progress .progress-bar").css({ "width": "0%" });

    })
   $(this).on('submit', '.formularioFile',function(e) {

    e.preventDefault();

    var inputFileImage = document.getElementById("archivo_id");
    var file = inputFileImage.files[0];
    var data = new FormData();
    var barra = $('.progress-bar');
    var url = $(this).attr('action');

    data.append('archivo',file);
    var url = url;
    $.ajax({
      url:url,
      type:'POST',
      contentType:false,
      data:data,
      processData:false,
      cache:false,
      async: true,
      xhr: function () {
        var xhr = new window.XMLHttpRequest();
        //Progrerso de subida
        xhr.upload.addEventListener("progress", function (evt) {
          if (evt.lengthComputable) {
            var percentComplete = parseInt((evt.loaded / evt.total) * 100);
            $('div.progress > div.progress-bar').css({ "width": percentComplete + "%" });
          }
        }, false);

        //progreso de bajada
        xhr.addEventListener("progress", function (evt) {
          if (evt.lengthComputable) {
            var percentComplete = (evt.loaded / evt.total) *100;
            $("#progress .progress-bar").css({ "width": percentComplete + "%" });
          }
        },false);
        return xhr;
      },
      success: function(data) {
        $('.mensaje').html('<div class="alert alert-warning">' + data + '</div>');
      },
      error: function(data) {
        $('.mensaje').html('<div class="alert alert-success">Error al Subir el archivo, intentelo de nuevo...</div>');
      }
    });

    });



})

  function load(page, liga) {

    $('#loader').fadeIn('slow');

    $.ajax({
      url:liga,
      data: 'action=ajax&page='+page,
      beforeSend: function(objeto) {
        //alert(objeto);
        //$('#loader').html("<img class='fa fa-search'>");
      },
      success:function(data) {
        //alert(data);
        $(".outer").html(data).fadeIn('slow');
        $('#loader').html(data);
      }
    })
  }

function lcontainer(contenido, url) {

    $.ajax({

        url:url,
        beforeSend: function(objeto) {

        },
        success:function(data) {

            $('.contenido-admin').hide();
            $(contenido).html(data);
            $(contenido).show();

        }

    })
}

function formulario(type, action, dat) {

    $.ajax({

        type: type,
        url: action,
        data: dat,
        success: function(data) {

            $('#result').html(data);
            sweetAlert({title:'Mensaje',text:'¡Registro dado de alta!' + data + '',timer:3000,showConfirmButton:false,type:'success'});

        }

    })
}

function confirRegistros(parametros, url) {

    $.post(url,parametros);
    location.reload(true);

}